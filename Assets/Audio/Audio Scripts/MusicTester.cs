using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTester : MonoBehaviour
{
    public AUDIO_ID music;
    // Start is called before the first frame update
    void Start()
    {
        AudioMaster.PlayMusic(music);
    }
}
