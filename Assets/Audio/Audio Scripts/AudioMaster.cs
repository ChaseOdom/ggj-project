using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioMaster : MonoBehaviour
{
    static AudioPlayer musicPlayer;
    static bool musicPlayerInitialized;

    public static void OnPause()
    {
        GetMixer(AUDIO_BUS.MASTER).audioMixer.FindSnapshot("Paused").TransitionTo(0f);
        AudioPlayer.PauseAllDirectional();
    }
    public static void OnResume()
    {
        GetMixer(AUDIO_BUS.MASTER).audioMixer.FindSnapshot("Default").TransitionTo(0f);
        AudioPlayer.UnPauseAllDirectional();
    }
    public static void SetVolume(AUDIO_BUS bus, float volume)
    {
        volume = Mathf.Clamp(volume, 0.0001f, 1f);
        var mixer = GetMixer(bus);
        mixer.audioMixer.SetFloat(bus.ToString() + "VOLUME", RemapVolume(volume));
    }
    public static void PlayMusic(AUDIO_ID id)
    {
        if (!musicPlayerInitialized)
        {
            musicPlayerInitialized = true;
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged += (_, __) => musicPlayer = null;
        }

        if (musicPlayer != null)
        {
            musicPlayer.Stop();
        }
        musicPlayer = PlayGlobal(id, AUDIO_BUS.MUSIC, true);
    }
    public static AudioPlayer PlayGlobal(AUDIO_ID id, AUDIO_BUS bus, bool loop = false)
    {
        var player = AudioPlayer.GetNewPlayer();
        var mixer = GetMixer(bus);
        var asset = GetAsset(id);
        return player.PlayGlobal(asset, mixer, loop);
    }
    public static AudioPlayer PlayAt(AUDIO_ID id, AUDIO_BUS bus, Vector3 position, Transform parent = null, bool loop = false)
    {
        var player = AudioPlayer.GetNewPlayer();
        var mixer = GetMixer(bus);
        var asset = GetAsset(id);
        return player.PlayAt(asset, mixer, parent, position, loop);
    }


    static Dictionary<AUDIO_BUS, AudioMixerGroup> mixers;
    static Dictionary<AUDIO_ID, AudioAsset> audioAssets;
    static AudioMixerGroup GetMixer(AUDIO_BUS bus)
    {
        InitializeMixerDict();
        return mixers[bus];
    }

    static float RemapVolume(float volume) => Mathf.Log10(volume) * 20;
    static void InitializeMixerDict()
    {
        if (mixers == null)
        {
            mixers = new Dictionary<AUDIO_BUS, AudioMixerGroup>();
            mixers[AUDIO_BUS.MASTER] = Prefabs.library.masterMixer.FindMatchingGroups("Master")[0];
            mixers[AUDIO_BUS.MUSIC] = Prefabs.library.masterMixer.FindMatchingGroups("Master/Music")[0];
            mixers[AUDIO_BUS.SFX] = Prefabs.library.masterMixer.FindMatchingGroups("Master/SFX")[0];
        }
    }

    static void InitializeAudioAssets()
    {
        if (audioAssets == null)
        {
            audioAssets = new Dictionary<AUDIO_ID, AudioAsset>();
            var assets = Resources.LoadAll<AudioAsset>("Audio Assets/");
            foreach (var asset in assets)
            {
                audioAssets[asset.id] = asset;
            }
        }
    }
    static AudioAsset GetAsset(AUDIO_ID id)
    {
        InitializeAudioAssets();
        if (audioAssets.ContainsKey(id))
            return audioAssets[id];
        else
            return null;
    }
}

public enum AUDIO_BUS
{
    MASTER,
    MUSIC,
    SFX
}