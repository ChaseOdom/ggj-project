using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Audio Asset")]
public class AudioAsset : ScriptableObject
{
    [System.Serializable] public class Clip
    {
        public float weight = 1f;
        public AudioClip clip;
    }

    public AUDIO_ID id;
    [SerializeField] Clip[] clips;
    [Range(-3, 3)] [SerializeField] float minPitch = 1f;
    [Range(-3, 3)] [SerializeField] float maxPitch = 1f;
    public float randomPitch => Random.Range(minPitch, maxPitch);

    public Clip GetRandomClip()
    {
        float totalWeight = 0f;
        for (int i = 0; i < clips.Length; ++i)
        {
            totalWeight += clips[i].weight;
        }

        if (totalWeight == 0f)
        {
            InitWeights();
            totalWeight = clips.Length;
        }

        float chosen = Random.Range(0f, totalWeight);
        float runningTotal = 0f;
        for (int i = 0; i < clips.Length; ++i)
        {
            if (runningTotal + clips[i].weight >= chosen)
                return clips[i];
            runningTotal += clips[i].weight;
        }

        Debug.LogError($"No clip could be selected for {id}");
        return null;
    }

    void InitWeights()
    {
        for (int i = 0; i < clips.Length; ++i)
        {
            clips[i].weight = 1f;
        }
    }
}

