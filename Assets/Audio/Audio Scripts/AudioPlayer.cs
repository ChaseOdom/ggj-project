using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioPlayer : MonoBehaviour
{
    [Header("References")]
    [SerializeField] AudioSource source;

    bool playing;
    bool paused;

    public AudioPlayer PlayAt(AudioAsset asset, AudioMixerGroup group, Transform parent, Vector3 position, bool loop)
    {
        source.spatialBlend = 1f;
        transform.parent = parent;
        transform.position = position;
        activeDirectionalPlayers.Add(this);
        return Play(asset, group, loop);
    }
    public AudioPlayer PlayGlobal(AudioAsset asset, AudioMixerGroup group, bool loop)
    {
        source.spatialBlend = 0f;
        transform.parent = null;
        return Play(asset, group, loop);
    }
    AudioPlayer Play(AudioAsset asset, AudioMixerGroup group, bool loop)
    {
        if (asset == null)
        {
            Reclaim(this);
            return null;
        }
        source.outputAudioMixerGroup = group;
        var chosenClip = asset.GetRandomClip();
        source.pitch = asset.randomPitch;
        source.clip = chosenClip.clip;
        source.loop = loop;
        source.Play();
        playing = true;

        return this;
    }

    public void Pause()
    {
        if (playing)
        {
            source.Pause();
            paused = true;
        }
    }
    public void UnPause()
    {
        if (playing && paused)
        {
            source.UnPause();
            paused = false;
        }
    }
    public void Stop()
    {
        if (playing)
        {
            source.Stop();
            playing = false;
            Reclaim(this);
        }
    }

    private void Update()
    {
        if (playing)
        {
            if (!source.isPlaying && !paused)
                Reclaim(this);
        }
    }

    //static/pooling functions
    static Stack<AudioPlayer> pool;
    static HashSet<AudioPlayer> activeDirectionalPlayers;

    static void InitializePool()
    {
        if (pool == null)
        {
            pool = new Stack<AudioPlayer>();
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged += (_, __) => pool.Clear();

            activeDirectionalPlayers = new HashSet<AudioPlayer>();
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged += (_, __) => activeDirectionalPlayers.Clear();
        }
    }

    public static AudioPlayer GetNewPlayer()
    {
        InitializePool();

        if (pool.Count > 0)
        {
            var player = pool.Pop();
            if (!player)
            {
                pool.Clear();
                activeDirectionalPlayers.Clear();
                return GetNewPlayer();
            }
            player.gameObject.SetActive(true);
            return player;
        }
        else
        {
            return Instantiate(Prefabs.library.audioPlayerPrefab);
        }
    }

    static void Reclaim(AudioPlayer player)
    {
        InitializePool();

        player.transform.parent = null;
        player.playing = false;
        player.paused = false;
        player.gameObject.SetActive(false);
        activeDirectionalPlayers.Remove(player);
        pool.Push(player);
    }

    public static void PauseAllDirectional()
    {
        InitializePool();

        foreach (var player in activeDirectionalPlayers)
        {
            player.Pause();
        }
    }
    public static void UnPauseAllDirectional()
    {
        InitializePool();

        foreach (var player in activeDirectionalPlayers)
        {
            player.UnPause();
        }
    }
}
