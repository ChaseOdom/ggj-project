using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTester : MonoBehaviour
{
    public AUDIO_ID audioID;
    public AUDIO_BUS audioBus;
    public bool spatial;
    public bool play;
    public bool loop;

    AudioPlayer loopingSource;

    private void Update()
    {
        if (play && loopingSource == null)
        {
            AudioPlayer source = null;
            if (spatial)
                source = AudioMaster.PlayAt(audioID, audioBus, transform.position, transform, loop);
            else
                source = AudioMaster.PlayGlobal(audioID, audioBus, loop);

            if (loop)
                loopingSource = source;
            else
                play = false;
        }
        else if (!play && loopingSource != null)
        {
            loopingSource.Stop();
            loopingSource = null;
        }
    }
}
