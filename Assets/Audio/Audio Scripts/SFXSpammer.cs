using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXSpammer : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(SpamSFX());
    }
    IEnumerator SpamSFX()
    {
        while(true)
        {
            yield return new WaitForSeconds(Random.Range(1f, 5f));
            AudioMaster.PlayAt(AUDIO_ID.TEST_SOUND, AUDIO_BUS.SFX, transform.position);
        }
    }
}
