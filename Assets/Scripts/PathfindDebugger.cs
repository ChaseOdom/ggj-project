using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindDebugger : MonoBehaviour
{
    public Transform destination;


#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (DungeunUtils.instance && DungeunUtils.instance.pathfindingBaked)
        {
            var myTile = DungeunUtils.instance.GetTile(transform.position);
            var destTile = DungeunUtils.instance.GetTile(destination.position);
            var myRoom = DungeunUtils.instance.GetRoom(transform.position);
            var destRoom = DungeunUtils.instance.GetRoom(destination.position);

            if (myTile == null || destTile == null)
            {
                Debug.Log("!!!!");
                return;
            }

            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(DungeunUtils.instance.TileCenter(myTile), 0.3f);
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(DungeunUtils.instance.TileCenter(destTile), 0.3f);
            if (myTile != destTile)
            {
                Tile next = DungeunUtils.instance.GetNextTile(myTile, destTile);
                if (next != null) {
                    Gizmos.color = Color.red;
                    Gizmos.DrawSphere(DungeunUtils.instance.TileCenter(next), 0.3f);
                }
            }
        }
    }
#endif
}
