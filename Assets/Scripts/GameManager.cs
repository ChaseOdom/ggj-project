using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [System.Serializable] public class StateInfo
    {
        public GameState stateID;
        public GameObject[] activatedObjects;
        public UnityEngine.Events.UnityEvent onStateEnter;
        public UnityEngine.Events.UnityEvent onStateExit;
    }

    [SerializeField] List<StateInfo> states;
    Dictionary<GameState, StateInfo> stateInfoDict;

    [SerializeField] PlayerResourceManager resourceManager;

    public bool isMainMenu;

    StateInfo currState;
    static GameManager instance;

    private void Awake()
    {
        instance = this;
        stateInfoDict = new Dictionary<GameState, StateInfo>();
        foreach (var info in states)
        {
            stateInfoDict[info.stateID] = info;
        }

        if (isMainMenu)
        {
            DungeunUtils.instance.BakePathfindingGrid();
            ChangeState(GameState.DUNGEON_DEFENSE);
        }
        else
        {
            StartCoroutine(GameLoop());
        }
    }

    public static void ChangeGameState(GameState newState) => instance.ChangeState(newState);
    public static GameState currGameState => instance.currState.stateID;
    void ChangeState(GameState newState)
    {
        if (currState != null)
        {
            currState.onStateExit.Invoke();
            foreach (var obj in currState.activatedObjects)
                obj.SetActive(false);
        }

        currState = stateInfoDict[newState];
        foreach (var obj in currState.activatedObjects)
            obj.SetActive(true);
        currState.onStateEnter.Invoke();
    }

    IEnumerator GameLoop()
    {
        AudioMaster.PlayMusic(AUDIO_ID.MAIN_MUSIC);
        ChangeState(GameState.EDIT_DUNGEON);
        yield return null;
        DungeunUtils.instance.BakePathfindingGrid();

        TutorialPopup.ShowMessage("Protect your dungeon from invaders so you can sell them back their lost gear later! Click on the room with the eggs to spawn some dinosaurs, but keep in mind it uses eggs and dinosaurs despawn between rounds. Click the bell room to summon nearby dinos. Don't let the adventurers steal your loot!", Vector2.zero, TutorialPopup.dismissOnClickCondition);

        ChangeState(GameState.DUNGEON_DEFENSE);
        while (AdventurerSpawner.waveInProgress)
            yield return null;
        resourceManager.DoLootTally();
        while (DungeonResourceDrop.activeLoot != null && DungeonResourceDrop.activeLoot.Count > 0)
            yield return null;
        if (resourceManager.GetResource("Gold").val == 0 && resourceManager.GetResource("Gear").val == 0)
            ChangeState(GameState.DEFEAT);
        else
        {
            yield return new WaitForSeconds(1.5f);
            TutorialPopup.ShowMessage("Good job! Welcome to the dungeon lost and found shop, where you can sell adventurers back the gear they lost. You can also restock eggs here, and you will use gold and gear to build new dungeon rooms on the next screen.", Vector2.zero, TutorialPopup.dismissOnClickCondition);
        }
        bool firstEdit = true;
        //defense
        //main loop
        while (currState.stateID != GameState.DEFEAT)
        {
            ChangeState(GameState.TRADE);
            while (currState.stateID == GameState.TRADE)
                yield return null;

            if (firstEdit)
            {
                firstEdit = false;
                TutorialPopup.ShowMessage("Welcome to the dungeon editor! Here you can add new rooms and change the layout of your dungeon. Use the edit dungeon button to connect rooms with corridors by clicking and dragging in empty space, or to move existing rooms around.", Vector2.zero, TutorialPopup.dismissOnClickCondition);
            }

            ChangeState(GameState.EDIT_DUNGEON);
            while (currState.stateID == GameState.EDIT_DUNGEON)
                yield return null;
            while (AdventurerSpawner.waveInProgress)
                yield return null;
            resourceManager.DoLootTally();
            while (DungeonResourceDrop.activeLoot.Count > 0)
                yield return null;
            if (resourceManager.GetResource("Gold").val == 0 && resourceManager.GetResource("Gear").val == 0)
                ChangeState(GameState.DEFEAT);
            else
                yield return new WaitForSeconds(1.5f);
        }
        TutorialPopup.ShowMessage("Oops, looks like the adventurers stole all your loot! Try not to let that happen in the future.", Vector2.zero, TutorialPopup.dismissOnClickCondition);
        yield return new WaitForSeconds(15f);
        Application.Quit();
    }

    public void TheBigAsteroidHitsEarth()
    {
        foreach (var dinosaur in FindObjectsOfType<Dinosaur>())
        {
            dinosaur.health = 0;
        }
    }
}

public enum GameState
{
    EDIT_DUNGEON,
    DUNGEON_DEFENSE,
    HAGGLE,
    TRADE,
    DEFEAT
}