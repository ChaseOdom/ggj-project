using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DinosaurType
{
    COMPSOGNATHUS,
    VELOCIRAPTOR,
    TREX,
    PTERODACTYL,
    QUETZALCOATLUS,
    DILOPHOSAURUS,
    CUCKOO
}

public enum DinosaurStats
{
    HEALTH,
    SPEED,
    ATTACK,
    HEARING_RANGE,
    LOOT_CARRY,
    ATTACK_RANGE
}

public class DinosaurDatabase : MonoBehaviour
{
    public static float[,] dinoStatMatrix;
    public static HashSet<Dinosaur> dinosaursSpawned;
    public const int MAX_DINOS = 100;

    void Awake()
    {
        InitSpawns();
        if (dinoStatMatrix == null)
        {
            dinoStatMatrix = new float[,]
            { 
                // HEALTH, SPEED (tiles/sec), ATTACK, HEARING_RANGE, LOOT_CARRY, ATTACK_RANGE
                { 5, 1.5f, 0.5f, 10, 5, .3f }, // COMPSOGNATHUS
                { 10, 3f, 2, 25, 7, .4f }, // VELOCIRAPTOR
                { 50, 0.75f, 6, 8, 30, .8f }, // TREX
                { 15, 5f, 1, 15, 15, 1.2f }, // PTERODACTYL
                { 20, 2.5f, 4.5f, 20, 20, 10f }, // QUETZALCOATLUS
                { 17, 1.2f, 1.5f, 13, 12, 3f }, // DILOPHOSAURUS
                { 25, 3f, 10f, 40, 15, 20 } // CUCKOO
            };
        }
    }

    static void InitSpawns()
    {
        if (dinosaursSpawned == null)
        {
            dinosaursSpawned = new HashSet<Dinosaur>();
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged += (_, __) => dinosaursSpawned.Clear();
        }
    }

    public static void DinoSpawned(Dinosaur dino)
    {
        InitSpawns();
        dinosaursSpawned.Add(dino);
    }
    public static void DinoDied(Dinosaur dino)
    {
        InitSpawns();
        dinosaursSpawned.Remove(dino);
    }
}
