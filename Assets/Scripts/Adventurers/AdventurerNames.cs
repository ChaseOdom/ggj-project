using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Adventurer Name List")]
public class AdventurerNames : ScriptableObject
{
    public List<string> names;
}
