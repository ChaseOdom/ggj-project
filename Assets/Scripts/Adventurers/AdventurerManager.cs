using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdventurerManager : MonoBehaviour
{
    static List<string> unusedNames;
    static Dictionary<string, PersistentAdventurerData> adventurerData;

    public static void Initialize()
    {
        if (unusedNames == null)
        {
            unusedNames = new List<string>(Prefabs.library.adventurerNameList);
            adventurerData = new Dictionary<string, PersistentAdventurerData>();
        }
    }

    public static string GetUnusedName()
    {
        Initialize();

        if (unusedNames.Count == 0)
            return $"nameless adventurer #{Random.Range(0, short.MaxValue)}";

        int chosenName = Random.Range(0, unusedNames.Count);
        string name = unusedNames[chosenName];
        unusedNames.RemoveAt(chosenName);
        return name;
    }

    public static PersistentAdventurerData GetDataFor(string name)
    {
        Initialize();

        if (!adventurerData.ContainsKey(name))
            adventurerData[name] = new PersistentAdventurerData() { name = name };

        return adventurerData[name];
    }

    public static List<string> GetActiveAdventurerNames()
    {
        Initialize();

        return new List<string>(adventurerData.Keys);
    }
}

public class PersistentAdventurerData
{
    public string name;
    public int gold;
    public int lostGear;
}
