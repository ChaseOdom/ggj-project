using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Adventurer : MonoBehaviour
{
    public bool debugStateMachine;
    AdventurerAIState currState;
    Dictionary<AdventurerState, AdventurerAIState> states;

    [Header("References")]
    [SerializeField] Transform weaponAnchor;
    public DungeonPathfinder pathfinder;
    [SerializeField] Animator animator;

    [Header("Properties")]
    [SerializeField] Vector2 maxSanityRange;
    [SerializeField] float maxHP;
    [SerializeField] Weapon[] possibleStartWeapons;
    [SerializeField] int maxInventorySize;
    public string adventurerName;
    [SerializeField] float[] moveSpeeds;

    [HideInInspector] public Weapon equippedWeapon;

    public HashSet<Room> visitedRooms;
    public Tile entranceTile;

    float _currHP;
    public float currHP
    {
        get => _currHP;
        set
        {
            _currHP = value;
            if (value <= 0f)
                Kill();
            debugHP = value;
        }
    }
    float _currSanity;
    public float currSanity
    {
        get => _currSanity;
        set
        {
            _currSanity = value;
            if (value <= 0f)
                SendStateMessage(AdventurerMessage.SANITY_BROKEN);
        }
    }
    public bool dead
    {
        get;
        private set;
    }
    [HideInInspector] public int carriedGear;
    [HideInInspector] public int carriedGold;
    public bool inventoryFull => availableInventorySpace <= 0;
    public int availableInventorySpace => maxInventorySize - (carriedGear * 10 + carriedGold);

    public Tile myTile => DungeunUtils.instance ? DungeunUtils.instance.GetTile(transform.position) : null;
    public Room myRoom => DungeunUtils.instance ? DungeunUtils.instance.GetRoom(transform.position) : null;
    Room prevRoom;
    [HideInInspector] public Tile desiredTile;

    public float debugHP;
    [HideInInspector] public bool weaponRotationOverride;

    public List<ILootSource> nearbyLoot
    {
        get
        {
            if (myTile.isRoom)
                return myRoom.LootInRoom();
            else {
                var list = new List<ILootSource>();
                    list.AddRange(DungeonResourceDrop.activeLoot);
                return list.FindAll((e) => (e as DungeonResourceDrop).NearbyCorridorTile(myTile, 3));
                    }
        }
    }

    [ContextMenu("Kill")] void TestKill() => currHP = 0;

    private void Awake()
    {
        InitStates();
        currSanity = Random.Range(maxSanityRange.x, maxSanityRange.y);
        currHP = maxHP;
        visitedRooms = new HashSet<Room>();
    }

    private void Start()
    {
        equippedWeapon = Utility.RandomFromArray(possibleStartWeapons).Spawn(weaponAnchor.GetChild(0));
        carriedGear += equippedWeapon.gearDrops;
        ChangeStates(AdventurerState.IDLE);
        entranceTile = myTile;
    }

    public void Goto(Vector3 position, MoveSpeed speed)
    {
        pathfinder.destination = position;
        pathfinder.speed = moveSpeeds[(int)speed];
        animator.SetFloat("SpeedMultiplier", speed == MoveSpeed.RUN ? 2 : 1);
    }
    public void Stop()
    {
        pathfinder.speed = 0f;
    }

    public void Kill()
    {
        if (!dead)
        {
            dead = true;
            _currHP = 0;
            KillStateMachine();
            AdventurerSpawner.RemoveActiveAdventurer(this);
            if (myRoom != null) myRoom.AdventurerExited(this);
            if (adventurerName != "")
                AdventurerManager.GetDataFor(adventurerName).lostGear += equippedWeapon.gearDrops;
            DungeonResourceDrop.SpawnLoot(transform.position, carriedGold, carriedGear);
            gameObject.SetActive(false);
        }
    }

    public void FaceWeaponTowards(Vector3 target)
    {
        weaponAnchor.LookAt(target);
    }

    public void ChangeStates(AdventurerState newState)
    {
        if (currState != null)
            currState.OnStateExit();
        Stop();
        currState = states[newState];
        currState.OnStateEnter();
    }

    public void KillStateMachine()
    {
        if (currState != null)
            currState.OnStateExit();
        currState = null;
    }

    private void Update()
    {
        if (inventoryFull)
            SendStateMessage(AdventurerMessage.INVENTORY_FULL);

        var room = myRoom;
        if (prevRoom != room)
        {
            if (prevRoom != null)
                prevRoom.AdventurerExited(this);
            if (room != null)
            {
                room.AdventurerEntered(this);
                SendStateMessage(AdventurerMessage.ROOM_ENTERED);
                visitedRooms.Add(room);
            }
        }
        prevRoom = room;

        if (room != null && room.DinosaursInRoom().Count > 0)
            SendStateMessage(AdventurerMessage.DINO_SPOTTED);

        if (room != null && room.LootInRoom().Count > 0)
            SendStateMessage(AdventurerMessage.LOOT_SIGHTED);

        animator.SetInteger("Direction", pathfinder.moveDir);
        animator.SetBool("Walking", pathfinder.speed > 0 && !pathfinder.atDestination);
        if (!weaponRotationOverride)
        {
            float weaponRot = 0f;
            switch(pathfinder.moveDir)
            {
                case 0:
                    weaponRot = 270f;
                    break;
                case 1:
                    weaponRot = 0f;
                    break;
                case 2:
                    weaponRot = 90f;
                    break;
                case 3:
                    weaponRot = 180f;
                    break;
            }
            weaponAnchor.localEulerAngles = new Vector3(weaponRot, 90f);
        }


        if (currState != null)
            currState.OnStateUpdate();
    }
    
    public void ReachedDungeonExit()
    {
        if (adventurerName != "")
            AdventurerManager.GetDataFor(adventurerName).gold += carriedGold + carriedGear * 10;
        AdventurerSpawner.RemoveActiveAdventurer(this);
        if (myRoom != null) myRoom.AdventurerExited(this);
        gameObject.SetActive(false);
    }

    public void SendStateMessage(AdventurerMessage message)
    {
        if (currState != null)
            currState.OnMessageRecieved(message);
    }

    void InitStates()
    {
        if (states == null)
        {
            states = new Dictionary<AdventurerState, AdventurerAIState>();
            states[AdventurerState.IDLE] = AdventurerAIState.New<AdventurerIdleState>(AdventurerState.IDLE, this);
            states[AdventurerState.GOTO_ROOM] = AdventurerAIState.New<AdventurerGotoRoomState>(AdventurerState.GOTO_ROOM, this);
            states[AdventurerState.ATTACK_DINO] = AdventurerAIState.New<AdventurerAttackDinoState>(AdventurerState.ATTACK_DINO, this);
            states[AdventurerState.ATTACK_OBJECT] = AdventurerAIState.New<AdventurerAttackObjectState>(AdventurerState.ATTACK_OBJECT, this);
            states[AdventurerState.ESCAPE] = AdventurerAIState.New<AdventurerEscapeState>(AdventurerState.ESCAPE, this);
            states[AdventurerState.FLEE] = AdventurerAIState.New<AdventurerFleeState>(AdventurerState.FLEE, this);
            states[AdventurerState.LOOT] = AdventurerAIState.New<AdventurerLootState>(AdventurerState.LOOT, this);
        }
    }
}

public enum MoveSpeed
{
    STOP,
    WALK,
    RUN
}
