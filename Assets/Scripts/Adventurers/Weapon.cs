using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Animator animator;

    [Header("Stats")]
    public float damage;
    public float attackDelay;
    public int gearDrops;
    public float range;

    public Weapon Spawn(Transform parent)
    {
        var clone = Instantiate(this, parent);
        clone.transform.rotation = Quaternion.identity;
        return clone;
    }

    public void Attack(Dinosaur target)
    {
        target.health -= damage;
        animator.SetTrigger("Attack");
    }
}
