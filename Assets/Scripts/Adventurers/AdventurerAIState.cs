using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AdventurerState
{
    IDLE,
    GOTO_ROOM,
    ATTACK_DINO,
    ATTACK_OBJECT,
    FLEE,
    ESCAPE,
    LOOT
}

public enum AdventurerMessage
{
    TEST,
    SANITY_BROKEN,
    DINO_SPOTTED,
    OBJECT_SPOTTED,
    ROOM_ENTERED,
    INVENTORY_FULL,
    LOOT_SIGHTED
}

public abstract class AdventurerAIState
{
    protected Adventurer adventurer;
    AdventurerState state;
    protected Transform transform => adventurer.transform;

    public virtual void OnStateInitialized()
    {
        if (adventurer.debugStateMachine)
            Debug.Log($"{adventurer.gameObject.name} initializing state {state}");
    }

    public virtual void OnStateEnter()
    {
        if (adventurer.debugStateMachine)
            Debug.Log($"{adventurer.gameObject.name} entering state {state}");
    }

    public virtual void OnStateExit()
    {
        if (adventurer.debugStateMachine)
            Debug.Log($"{adventurer.gameObject.name} exiting state {state}");
    }

    public virtual void OnMessageRecieved(AdventurerMessage message)
    {
        if (adventurer.debugStateMachine)
            Debug.Log($"{adventurer.gameObject.name} handling message {message} in state {state}");
    }

    public virtual void OnStateUpdate()
    {

    }

    public static AdventurerAIState New<T>(AdventurerState state, Adventurer adventurer) where T: AdventurerAIState, new()
    {
        var stateObject = new T();
        stateObject.adventurer = adventurer;
        stateObject.state = state;
        stateObject.OnStateInitialized();
        return stateObject;
    }
}

/////////////////////////////////////////////////////////////IDLE STATE
public class AdventurerIdleState : AdventurerAIState 
{
    Coroutine thinkingRoutine;

    public override void OnMessageRecieved(AdventurerMessage message)
    {
        base.OnMessageRecieved(message);
        switch(message)
        {
            case AdventurerMessage.DINO_SPOTTED:
                adventurer.ChangeStates(AdventurerState.ATTACK_DINO);
                break;
            case AdventurerMessage.OBJECT_SPOTTED:
                adventurer.ChangeStates(AdventurerState.ATTACK_OBJECT);
                break;
            case AdventurerMessage.LOOT_SIGHTED:
                adventurer.ChangeStates(AdventurerState.LOOT);
                break;
            case AdventurerMessage.SANITY_BROKEN:
                adventurer.ChangeStates(AdventurerState.FLEE);
                break;
        }

    }

    IEnumerator Think()
    {
        yield return null;
        for (int i = 0; i < 3; ++i)
        {
            Vector3 wanderDest = FindWanderDest();
            adventurer.Goto(wanderDest, MoveSpeed.WALK);
            yield return new WaitForSeconds(Random.Range(1f, 1.9f));
        }
        List<Tile> neighbouringRooms = DungeunUtils.instance.FindConnectedDoorways(adventurer.myRoom);
        List<Tile> unvisitedNeighbouringRooms = neighbouringRooms.FindAll((e) => !adventurer.visitedRooms.Contains(e.room));
        if (unvisitedNeighbouringRooms.Count > 0)
            adventurer.desiredTile = Utility.RandomFromList(unvisitedNeighbouringRooms);
        else if (adventurer.visitedRooms.Count == DungeunUtils.instance.GetRooms().Count)
            adventurer.ChangeStates(AdventurerState.ESCAPE);
        else
            adventurer.desiredTile = Utility.RandomFromList(neighbouringRooms);
        thinkingRoutine = null;
        adventurer.ChangeStates(AdventurerState.GOTO_ROOM);
    }

    Vector3 FindWanderDest()
    {
        var myRoom = adventurer.myRoom;
        if (myRoom == null)
            return transform.position;

        Vector2 offset = Vector2.zero;
        for (int i = 0; i < 10; ++i)
        {
            offset.x = Random.Range(0.5f, 1.3f) * (Random.Range(0f, 1f) > 0.5f ? -1 : 1);
            offset.y = Random.Range(0.5f, 1.3f) * (Random.Range(0f, 1f) > 0.5f ? -1 : 1);
            var room = DungeunUtils.instance.GetRoom(transform.position + (Vector3)offset);
            if (room == myRoom)
                return transform.position + (Vector3)offset;
        }
        return transform.position;
    }
    public override void OnStateExit()
    {
        base.OnStateExit();
        if (thinkingRoutine != null)
        {
            adventurer.StopCoroutine(thinkingRoutine);
            thinkingRoutine = null;
        }
    }

    public override void OnStateEnter()
    {
        base.OnStateEnter();
        if (thinkingRoutine == null)
            thinkingRoutine = adventurer.StartCoroutine(Think());
    }
}
public class AdventurerGotoRoomState : AdventurerAIState
{
    Coroutine delaySwitch;
    public override void OnMessageRecieved(AdventurerMessage message)
    {
        base.OnMessageRecieved(message);
        switch (message)
        {
            case AdventurerMessage.ROOM_ENTERED:
                delaySwitch = adventurer.StartCoroutine(SwitchAfterDelay(AdventurerState.IDLE, 1f));
                break;
            case AdventurerMessage.DINO_SPOTTED:
                adventurer.ChangeStates(AdventurerState.ATTACK_DINO);
                break;
            case AdventurerMessage.OBJECT_SPOTTED:
                adventurer.ChangeStates(AdventurerState.ATTACK_OBJECT);
                break;
            case AdventurerMessage.LOOT_SIGHTED:
                adventurer.ChangeStates(AdventurerState.LOOT);
                break;
            case AdventurerMessage.SANITY_BROKEN:
                adventurer.ChangeStates(AdventurerState.FLEE);
                break;
        }
    }

    IEnumerator SwitchAfterDelay(AdventurerState state, float delay)
    {
        yield return new WaitForSeconds(delay);
        delaySwitch = null;
        adventurer.ChangeStates(state);
    }

    public override void OnStateEnter()
    {
        base.OnStateEnter();
        adventurer.Goto(DungeunUtils.instance.TileCenter(adventurer.desiredTile), MoveSpeed.WALK);
    }

    public override void OnStateExit()
    {
        base.OnStateExit();
        if (delaySwitch != null)
        {
            adventurer.StopCoroutine(delaySwitch);
            delaySwitch = null;
        }
    }
}
public class AdventurerAttackDinoState : AdventurerAIState
    {
    Dinosaur target;
    Coroutine attackingRoutine;
        public override void OnMessageRecieved(AdventurerMessage message)
        {
            base.OnMessageRecieved(message);
            switch (message)
            {
                case AdventurerMessage.SANITY_BROKEN:
                    adventurer.ChangeStates(AdventurerState.FLEE);
                    break;
            }
        }

    public override void OnStateUpdate()
    {
        base.OnStateUpdate();
        if (!target)
            SelectTarget();
        else
        {
            if (Utility.VectorMagnitudeLTE(target.transform.position - transform.position, adventurer.equippedWeapon.range)) {
                if (attackingRoutine == null)
                {
                    adventurer.Stop();
                    attackingRoutine = adventurer.StartCoroutine(Attack());
                }
            }
            else
            {
                if (attackingRoutine != null)
                {
                    adventurer.StopCoroutine(attackingRoutine);
                    attackingRoutine = null;
                }
                adventurer.Goto(target.transform.position, MoveSpeed.RUN);
            }

            adventurer.FaceWeaponTowards(target.transform.position);
        }
    }

    IEnumerator Attack()
    {
        while(target)
        {
            yield return new WaitForSeconds(adventurer.equippedWeapon.attackDelay);
            if (target)
            {
                adventurer.equippedWeapon.Attack(target);
            }
        }
        attackingRoutine = null;
    }

    public override void OnStateEnter()
    {
        SelectTarget();
    }

    public override void OnStateExit()
    {
        base.OnStateExit();
        if (attackingRoutine != null)
        {
            adventurer.StopCoroutine(attackingRoutine);
            attackingRoutine = null;
        }
    }

    void SelectTarget()
    {
        var targets = adventurer.myRoom.DinosaursInRoom();
        if (targets.Count == 0)
        {
            if (adventurer.inventoryFull)
                adventurer.ChangeStates(AdventurerState.ESCAPE);
            else
                adventurer.ChangeStates(AdventurerState.IDLE);
        }
        else
        {
            target = Utility.RandomFromList(targets);
        }
    }
}
public class AdventurerAttackObjectState : AdventurerAIState
    {
        public override void OnMessageRecieved(AdventurerMessage message)
        {
            base.OnMessageRecieved(message);
            switch (message)
            {
                case AdventurerMessage.DINO_SPOTTED:
                    adventurer.ChangeStates(AdventurerState.ATTACK_DINO);
                    break;
                case AdventurerMessage.SANITY_BROKEN:
                    adventurer.ChangeStates(AdventurerState.FLEE);
                    break;
            }
        }
    }
public class AdventurerFleeState : AdventurerAIState
{
    public override void OnStateEnter()
    {
        base.OnStateEnter();
        adventurer.Goto(adventurer.entranceTile.worldCenter, MoveSpeed.RUN);
    }
    public override void OnStateUpdate()
    {
        base.OnStateUpdate();
        if (adventurer.pathfinder.atDestination)
        {
            adventurer.ReachedDungeonExit();
        }
    }
}
public class AdventurerEscapeState : AdventurerAIState
    {
        public override void OnMessageRecieved(AdventurerMessage message)
        {
            base.OnMessageRecieved(message);
            switch (message)
            {
                case AdventurerMessage.DINO_SPOTTED:
                    adventurer.ChangeStates(AdventurerState.ATTACK_DINO);
                    break;
                case AdventurerMessage.OBJECT_SPOTTED:
                    adventurer.ChangeStates(AdventurerState.ATTACK_OBJECT);
                    break;
                case AdventurerMessage.SANITY_BROKEN:
                    adventurer.ChangeStates(AdventurerState.FLEE);
                    break;
            }
    }
    public override void OnStateEnter()
    {
        base.OnStateEnter();
        adventurer.Goto(adventurer.entranceTile.worldCenter, MoveSpeed.WALK);
    }

    public override void OnStateUpdate()
    {
        base.OnStateUpdate();
        if (adventurer.pathfinder.atDestination)
        {
            adventurer.ReachedDungeonExit();
        }
    }
}
public class AdventurerLootState : AdventurerAIState
    {
    ILootSource chosenSource;
    Transform chosenSourceLocation => (chosenSource as MonoBehaviour).transform;
    Coroutine lootRoutine;

        public override void OnMessageRecieved(AdventurerMessage message)
        {
            base.OnMessageRecieved(message);
            switch (message)
            {
                case AdventurerMessage.DINO_SPOTTED:
                    adventurer.ChangeStates(AdventurerState.ATTACK_DINO);
                    break;
                case AdventurerMessage.OBJECT_SPOTTED:
                    adventurer.ChangeStates(AdventurerState.ATTACK_OBJECT);
                    break;
                case AdventurerMessage.INVENTORY_FULL:
                    adventurer.ChangeStates(AdventurerState.ESCAPE);
                    break;
                case AdventurerMessage.SANITY_BROKEN:
                    adventurer.ChangeStates(AdventurerState.FLEE);
                    break;
            }
        }

    public override void OnStateEnter()
    {
        base.OnStateEnter();
        chosenSource = Utility.RandomFromList(adventurer.myRoom.LootInRoom());
        adventurer.Goto(chosenSourceLocation.position, MoveSpeed.RUN);
    }

    public override void OnStateUpdate()
    {
        base.OnStateUpdate();
        if (chosenSource == null || !chosenSourceLocation.gameObject.activeInHierarchy || (chosenSource.GoldAmount() == 0 && chosenSource.GearAmount() == 0))
        {
            CancelLootRoutine();
            if (adventurer.myRoom.LootInRoom().Count > 0)
                OnStateEnter();
            else
                adventurer.ChangeStates(AdventurerState.IDLE);
        }

        if (adventurer.pathfinder.atDestination && lootRoutine == null)
        {
            adventurer.StartCoroutine(Loot());
        }
    }

    public override void OnStateExit()
    {
        base.OnStateExit();
        CancelLootRoutine();
    }

    IEnumerator Loot()
    {
        while (chosenSource != null && (chosenSource.GearAmount() > 0 || chosenSource.GoldAmount() > 0))
        {
            yield return new WaitForSeconds(1);
            bool takeGold = Random.Range(0f, 1f) > 0.5f;
            if (chosenSource != null)
            {
                if (chosenSource.GearAmount() > 0 && (!takeGold || chosenSource.GoldAmount() == 0))
                {
                    adventurer.carriedGear += chosenSource.TakeGear(1);
                }
                else
                {
                    adventurer.carriedGold += chosenSource.TakeGold(10);
                }
            }
        }
        lootRoutine = null;
    }

    void CancelLootRoutine()
    {
        if (lootRoutine != null)
        {
            adventurer.StopCoroutine(lootRoutine);
            lootRoutine = null;
        }
    }
}