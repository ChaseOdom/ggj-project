using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdventurerSpawner : MonoBehaviour
{
    List<string> availableAdventurers;
    List<Transform> spawnPoints;
    HashSet<Adventurer> activeAdventurers;
    static AdventurerSpawner instance;
    bool spawning;
    public static bool waveInProgress => instance.activeAdventurers == null || instance.activeAdventurers.Count > 0 || instance.spawning;
    [SerializeField] Wave[] waves;
    int waveNum = 0;

    public void ResetAvailable()
    {
        if (availableAdventurers == null)
            availableAdventurers = new List<string>();
        availableAdventurers.Clear();
        availableAdventurers.AddRange(AdventurerManager.GetActiveAdventurerNames());
    }

    private void Awake()
    {
        ResetAvailable();
        activeAdventurers = new HashSet<Adventurer>();
        instance = this;
    }

    private void Start()
    {
        FetchSpawnPoints();
    }

    public void DoWave()
    {
        int waveActualIdx = waveNum >= waves.Length ? waves.Length - 1 : waveNum;
        var wave = waves[waveActualIdx];
        spawning = true;
        StartCoroutine(DoGroupSpawns(wave.minGroupSize, wave.maxGroupSize, wave.groupCount + waveNum - waveActualIdx, wave.groupDelay));
    }

    IEnumerator DoGroupSpawns(int minSize, int maxSize, int groupCount, float delay)
    {
        for (int i = 0; i < groupCount; ++i)
        {
            int groupSize = Random.Range(minSize, maxSize);
            SpawnAdventurers(groupSize);
            yield return new WaitForSeconds(delay);
        }
        spawning = false;
        waveNum++;
    }

    public void SpawnAdventurers(int count)
    {
        if (DungeunUtils.instance && !DungeunUtils.instance.pathfindingBaked)
            DungeunUtils.instance.BakePathfindingGrid();

        for (int i = 0; i < count; ++i)
        {
            string chosenName = "";
            if (availableAdventurers.Count > 0)
            {
                int chosen = Random.Range(0, availableAdventurers.Count);
                chosenName = availableAdventurers[chosen];
                availableAdventurers.RemoveAt(chosen);
            }
            else
            {
                chosenName = GenerateNewAdventurer();
            }

            Transform spawnPoint = Utility.RandomFromList(spawnPoints);
            SpawnAdventurer(AdventurerManager.GetDataFor(chosenName), spawnPoint);
        }
    }

    string GenerateNewAdventurer()
    {
        var name = AdventurerManager.GetUnusedName();
        var data = AdventurerManager.GetDataFor(name);
        data.gold = 30; //placeholder
        return name;
    }

    void SpawnAdventurer(PersistentAdventurerData data, Transform parent)
    {
        Adventurer newAdventurer = Instantiate(Prefabs.library.adventurerPrefab, parent);
        newAdventurer.name = data.name;
        activeAdventurers.Add(newAdventurer);
    }

    public void FetchSpawnPoints()
    {
        if (spawnPoints == null)
            spawnPoints = new List<Transform>();
        spawnPoints.Clear();

        foreach (var spawnRoom in DungeunUtils.instance.GetRoomsOfType(RoomType.ENTRANCE))
        {
            spawnPoints.Add(spawnRoom.transform.Find("SpawnPoint"));
        }
    }

    [ContextMenu("Test Spawn")]
    public void TestSpawn() => SpawnAdventurers(1);

    public static List<Adventurer> ActiveAdventurers()
    {
        if (instance)
            return new List<Adventurer>(instance.activeAdventurers);
        return new List<Adventurer>();
    }
    public static void RemoveActiveAdventurer(Adventurer adventurer)
    {
        if (instance)
            if (instance.activeAdventurers.Contains(adventurer))
                instance.activeAdventurers.Remove(adventurer);
    }
}

[System.Serializable]
public class Wave
{
    public int minGroupSize;
    public int maxGroupSize;
    public int groupCount;
    public float groupDelay;
}