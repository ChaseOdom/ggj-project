using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonPathfinder : MonoBehaviour
{
    public float speed;
    public Vector3 destination;
    public float tolerance;
    Tile tempDest;
    Tile prevDestTile;
    [HideInInspector] public int moveDir;
    [HideInInspector] public bool atDestination;

    private void LateUpdate()
    {
        atDestination = false;
        if (speed > 0 && DungeunUtils.instance && DungeunUtils.instance.pathfindingBaked)
        {
            var myTile = DungeunUtils.instance.GetTile(transform.position);
            var destTile = DungeunUtils.instance.GetTile(destination);
            var myRoom = DungeunUtils.instance.GetRoom(transform.position);
            var destRoom = DungeunUtils.instance.GetRoom(destination);

            if (myTile == null || destTile == null)
                return;
            if (myTile.isBackground || destTile.isBackground)
                return;

            Vector2 direction = Vector2.zero;

            if (myTile == destTile)
            {
                if (!Utility.VectorMagnitudeLTE(destination - transform.position, tolerance))
                    direction = (destination - transform.position);
                else
                    atDestination = true;
            }
            else
            {
                if (myRoom != null && myRoom == destRoom)
                {
                    direction = (destination - transform.position);
                }
                else
                {
                    if (tempDest == null || prevDestTile != destTile)
                        tempDest = DungeunUtils.instance.GetNextTile(myTile, destTile);

                    if (myTile == tempDest && Utility.VectorMagnitudeLTE(DungeunUtils.instance.TileCenter(tempDest) - transform.position, tolerance))
                        tempDest = DungeunUtils.instance.GetNextTile(myTile, destTile);

                    if (tempDest == null)
                        return;

                    direction = (DungeunUtils.instance.TileCenter(tempDest) - transform.position);
                }
            }

            if (direction == Vector2.zero)
                return;

            CalculateMoveDir(direction);

            transform.position += (Vector3)direction.normalized * speed * Time.deltaTime;
            prevDestTile = destTile;

            if(direction.x > 0 && transform.rotation.eulerAngles.y != 0f)
            {
                transform.SetPositionAndRotation(transform.position, Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, 0f, transform.rotation.eulerAngles.z)));
            } else if (direction.x < 0 && transform.rotation.y != 180f)
            {
                transform.SetPositionAndRotation(transform.position, Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, 180f, transform.rotation.eulerAngles.z)));
            }
        }
    }

    void CalculateMoveDir(Vector2 direction)
    {
        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
        {
            if (direction.x > 0)
                moveDir = 1;
            else
                moveDir = 3;
        }
        else
        {
            if (direction.y > 0)
                moveDir = 0;
            else
                moveDir = 2;
        }
    }
}
