using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerResourceManager : MonoBehaviour
{
    public Resource[] resources;
    public GameObject resourceBar;
    public GameObject resourceDisplay;
    public static PlayerResourceManager instance;

    private void Awake()
    {
        instance = this;
    }

    public void Start()
    {
        foreach (Resource resource in resources)
        {
            GameObject newResource = Instantiate(resourceDisplay);
            resource.displayText = newResource.GetComponentInChildren<Text>();
            resource.iconTransform = newResource.transform as RectTransform;
            resource.LoadAmount();
            resource.displayText.text = resource.val.ToString();
            if (resource.info.resourceSprite != null)
                newResource.GetComponentInChildren<Image>().sprite = resource.info.resourceSprite;
            newResource.transform.parent = resourceBar.transform;
        }
    }

    public void SaveResourceCounts()
    {
        foreach (Resource resource in resources)
        {
            resource.SaveAmount();
        }
    }

    public void DoLootTally()
    {
        StartCoroutine(TallyLoot());
    }

    IEnumerator TallyLoot()
    {
        if (DungeonResourceDrop.activeLoot == null)
            yield break;
        Stack<DungeonResourceDrop> droppedLoot = new Stack<DungeonResourceDrop>(DungeonResourceDrop.activeLoot);
        var gold = GetResource("Gold");
        var gear = GetResource("Gear");
        while (droppedLoot.Count > 0)
        {
            var loot = droppedLoot.Pop();
            gold.Add(loot.goldAmount);
            ResourceEyeCandy.BurstEyeCandy(gold.info, Camera.main.WorldToScreenPoint(loot.transform.position), loot.goldAmount, gold.iconTransform);
            gear.Add(loot.gearAmount);
            ResourceEyeCandy.BurstEyeCandy(gear.info, Camera.main.WorldToScreenPoint(loot.transform.position), loot.gearAmount, gear.iconTransform);
            loot.SetVals(0, 0);
            yield return new WaitForSeconds(0.1f);
        }
    }

    public Resource GetResource(string name) {
        foreach (Resource resource in resources)
            if (name.Equals(resource.info.resourceName))
                return resource;
        return null;
    }

    public bool ContainsResource(string name)
    {
        if (GetResource(name) == null)
            return false;
        return true;
    }

    public int ResourceCount()
    {
        return resources.Length;
    }

    public int ResourceCountOfType(string resourceName)
    {
        foreach (Resource resource in resources)
        {
            if (resource.displayText.text.ToString().Equals(resourceName))
            {
                return resource.val;
            }
        }
        return 0;
    }
}

[System.Serializable]
public class Resource
{
    public ResourceInfo info;
    public int val;
    public Text displayText;
    public RectTransform iconTransform;

    static Dictionary<string, int> savedVals;

    public bool Consume(int cost)
    {
        if (cost > val)
        {
            Debug.Log("You do not have enough resources to consume" + cost + " " + info.resourceName);
            return false;
        }
        else if (cost < 1)
        {
            Debug.Log("You cannot consume negative amounts of a resource");
            return false;
        }

        val = val - cost;
        displayText.text = val.ToString();
        SaveAmount();
        return true;
    }

    public bool Add(int v)
    {
        if (v < 1)
        {
            Debug.Log("You cannot add negative amounts of a resource");
            return false;
        }
        else if (v == info.limit)
        {
            Debug.Log("You cannot add negative amounts of a resource");
            return false;
        }

        val = Mathf.Min(val + v, info.limit);
        displayText.text = val.ToString();
        SaveAmount();
        return true;
    }

    public void LoadAmount()
    {
        if (savedVals == null || !savedVals.ContainsKey(info.resourceName))
            val = info.startingCount;
        else
            val = savedVals[info.resourceName];
    }

    public void SaveAmount()
    {
        if (savedVals == null)
            savedVals = new Dictionary<string, int>();
        savedVals[info.resourceName] = val;
    }

    public static void ClearSavedAmounts()
    {
        if (savedVals != null)
            savedVals.Clear();
    }
}