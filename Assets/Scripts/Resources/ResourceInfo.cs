using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Resource")]
public class ResourceInfo : ScriptableObject
{
    public string resourceName;
    public int limit;
    public int startingCount;
    public Sprite resourceSprite;
    public Sprite resourcePickupSprite;
    public int marketBuyPrice;
    public int marketSellPrice;
    public float eyeCandyScale;
    public AUDIO_ID collectSound;
}
