using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HaggleManager : MonoBehaviour
{
    public PlayerResourceManager rManager;
    public List<string> adventurers;
    public double desperationModifier;
    public Text requestText;
    public InputField offerPrice;
    public InputField offerUnits;
    public int adventurerIndex;
    public int sellAttempts;

    public Resource targetResource;
    public Image display;
    public Image adventurerDisplay;
    public int lowPrice;
    public int highPrice;

    public Button nextAdventurerButton;
    public Button sellButton;
    void Start()
    {
        targetResource = rManager.GetResource("Gear");
        display.sprite = targetResource.info.resourceSprite;
        processAdventurers();
    }

    private void OnEnable()
    {
        targetResource = rManager.GetResource("Gear");
        display.sprite = targetResource.info.resourceSprite;
        adventurerDisplay.enabled = true;
        processAdventurers();
    }

    public void processAdventurers()
    {
        adventurers.Clear();
        List<string> tmpadventurers = AdventurerManager.GetActiveAdventurerNames();
        foreach (string name in tmpadventurers)
        {
            PersistentAdventurerData a = AdventurerManager.GetDataFor(name);
            if (a.lostGear > 0)
            {
                adventurers.Add(name);
            }
        }

        if (adventurers.Count > 0)
        {
            adventurerIndex = 0;
            spawnAdventurer(adventurers[0]);
        }
        else
        {
            requestText.text = "<No one shows up to the Inn looking for things>";
            nextAdventurerButton.interactable = false;
            sellButton.interactable = false;
            adventurerDisplay.enabled = false;
        }
    }

    public void spawnAdventurer(string name)
    {
        sellAttempts = 0;
        PersistentAdventurerData a = AdventurerManager.GetDataFor(name);

        if (a.lostGear > 10)
        {
            requestText.text = "Nononono.... I-I've lost everything! My grandfather's sword, my magic rings... Please! I'll do anything, just get them back for me!";
            desperationModifier = 3;
        }
        else if (a.lostGear > 8)
        {
            requestText.text = "Please! Tell me you found some of my stuff! They... my entire party... I can't go back in there!";
            desperationModifier = 2;
        }
        else if (a.lostGear > 4)
        {
            requestText.text = "Jeez, what the hell man?! Why didn't you warn us its that dangerous in there? Fuck! Get my stuff back and I'll pay you, I have to go report this!";
            desperationModifier = 1.25;
        }
        else if (a.lostGear > 2)
        {
            requestText.text = "Man, whats with this dungeon? These dinosaurs are rough! Did you find any of my gear?";
            desperationModifier = 0.75;
        }
        else
        {
            requestText.text = "Hey innskeep! This dungeons pretty cool! Nothing too hard for me, but I think I dropped some stuff in it. Anyone find anything?";
            desperationModifier = 0.5;
        }

        lowPrice = (int)(targetResource.info.marketSellPrice + (targetResource.info.marketSellPrice * desperationModifier) / 2);
        highPrice = (int)(targetResource.info.marketSellPrice + (targetResource.info.marketSellPrice * desperationModifier));
    }

    public void spawnNextAdventurer() {
        if(adventurerIndex + 1 < adventurers.Count)
        {
            sellButton.interactable = true;
            adventurerIndex++;
            if (adventurerIndex + 1 >= adventurers.Count)
            {
                nextAdventurerButton.interactable = false;
            }
            spawnAdventurer(adventurers[adventurerIndex]);
        }
        
    }

    public void sell()
    {

        int price = int.Parse(offerPrice.text);
        int amt = int.Parse(offerUnits.text);

        int perUnitPrice = price / amt;

        if(amt > targetResource.val)
        {
            requestText.text = "You don't even enough items to do that!";
            return;
        }

        if (sellAttempts <= 1) {

            if (perUnitPrice <= lowPrice)
            {
                requestText.text = "Holy crap really? Yes, sure!";
                targetResource.Consume(amt);
                rManager.GetResource("Gold").Add(price);
                sellButton.interactable = false;
            }
            else if (perUnitPrice <= highPrice)
            {
                if (sellAttempts == 0)
                {
                    requestText.text = "Can't you go any cheaper?";
                }
                else
                {
                    requestText.text = "Fine, I'll pay for that.";
                    targetResource.Consume(amt);
                    rManager.GetResource("Gold").Add(price);
                    sellButton.interactable = false;
                }
            }
            else
            {
                if (sellAttempts == 0)
                {
                    requestText.text = "Can't you go any cheaper?";
                }
                else
                {
                    requestText.text = "Thats way too much. I'm good, thanks.";
                    sellButton.interactable = false;
                }
            }
        }
    }

    public void nextResource()
    {
        if (targetResource.info.name.Equals("Gear"))
        {
            targetResource = rManager.GetResource("Eggs");
            display.sprite = targetResource.info.resourceSprite;
        }
        else
        {
            targetResource = rManager.GetResource("Gear");
            display.sprite = targetResource.info.resourceSprite;
        }
    }

    public void done()
    {
        this.gameObject.SetActive(false);
    }

}
