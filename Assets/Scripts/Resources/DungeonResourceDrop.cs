using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonResourceDrop : MonoBehaviour, ILootSource
{
    [Header("References")]
    [SerializeField] SpriteRenderer gearRenderer;
    [SerializeField] SpriteRenderer goldRenderer;

    [Header("Properties")]
    [SerializeField] Sprite[] gearSprites;
    [SerializeField] Sprite[] goldSprites;
    [SerializeField] int[] gearThresholds;
    [SerializeField] int[] goldThresholds;

    [HideInInspector] public int gearAmount;
    [HideInInspector] public int goldAmount;

    Tile tile;

    public void RefreshSprites()
    {
        gearRenderer.sprite = null;
        for (int i = 0; i < gearThresholds.Length; ++i)
        {
            if (gearAmount >= gearThresholds[i])
                gearRenderer.sprite = gearSprites[i];
        }
        goldRenderer.sprite = null;
        for (int i = 0; i < goldThresholds.Length; ++i)
        {
            if (goldAmount >= goldThresholds[i])
                goldRenderer.sprite = goldSprites[i];
        }
    }

    public int TakeGold(int amount)
    {
        if (amount > goldAmount)
            amount = goldAmount;
        goldAmount -= amount;
        RefreshSprites();
        CheckVals();
        return amount;
    }
    public int TakeGear(int amount)
    {
        if (amount > gearAmount)
            amount = gearAmount;
        gearAmount -= amount;
        RefreshSprites();
        CheckVals();
        return amount;
    }
    public int GoldAmount() => goldAmount;

    public int GearAmount() => gearAmount;

    public void Initialize(int gold, int gear, Tile tile)
    {
        SetVals(gold, gear);
        this.tile = tile;
        var room = tile.room;
        if (room != null)
        {
            room.AddLoot(this);
        }
    }

    public void SetVals(int gold, int gear)
    {
        goldAmount = gold;
        gearAmount = gear;
        RefreshSprites();
        CheckVals();
    }

    void CheckVals()
    {
        if (goldAmount == 0 && gearAmount == 0)
        {
            var room = tile.room;
            if (room != null)
            {
                room.RemoveLoot(this);
            }
            Reclaim(this);
        }
    }

    public bool NearbyCorridorTile(Tile corridorTile, int maxDistance)
    {
        if (!tile.isCorridor)
            return false;

        return DungeunUtils.instance.DistanceBetweenTiles(tile, corridorTile) <= maxDistance;
    }

    //pooling
    static Stack<DungeonResourceDrop> pool;
    public static HashSet<DungeonResourceDrop> activeLoot;

    static void InitializePool()
    {
        if (pool == null)
        {
            pool = new Stack<DungeonResourceDrop>();
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged += (_, __) => pool.Clear();
            activeLoot = new HashSet<DungeonResourceDrop>();
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged += (_, __) => activeLoot.Clear();
        }
    }

    static DungeonResourceDrop GetDrop()
    {
        InitializePool();
        if (pool.Count > 0)
        {
            var drop = pool.Pop();
            drop.gameObject.SetActive(true);
            return drop;
        }
        else
        {
            return Instantiate(Prefabs.library.resourceDropPrefab);
        }
    }

    public static void SpawnLoot(Vector3 location, int startingGold, int startingLoot)
    {
        var drop = GetDrop();
        drop.transform.position = location;
        var tile = DungeunUtils.instance.GetTile(location);
        drop.Initialize(startingGold, startingLoot, tile);
        activeLoot.Add(drop);
    }

    static void Reclaim(DungeonResourceDrop drop)
    {
        InitializePool();
        activeLoot.Remove(drop);
        pool.Push(drop);
        drop.gameObject.SetActive(false);
    }

}
