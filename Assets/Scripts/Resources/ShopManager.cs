using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    // Start is called before the first frame update
    public PlayerResourceManager rManager;
    public GameObject market;
    public GameObject buttonPairPrefab;

    public void OnDoneTrading()
    {
        GameManager.ChangeGameState(GameState.EDIT_DUNGEON);
    }
    void Start()
    {
        foreach (Resource r in rManager.resources)
        {
            if (!r.info.resourceName.Equals("Gold"))
            {
                GameObject buttonPair = Instantiate(buttonPairPrefab);
                buttonPair.transform.parent = market.transform;

                Button[] buttons = buttonPair.GetComponentsInChildren<Button>();
                buttons[0].onClick.AddListener(delegate { SellItem(r.info.resourceName); });
                buttons[0].GetComponentInChildren<Text>().text = $"Sell {r.info.resourceName} ({r.info.marketSellPrice}gp)";

                buttons[1].onClick.AddListener(delegate { BuyItem(r.info.resourceName); });
                buttons[1].GetComponentInChildren<Text>().text = $"Buy {r.info.resourceName} ({r.info.marketBuyPrice}gp)";
            }
        }
    }

    public void SellItem(string itemName)
    {
        Resource r = rManager.GetResource(itemName);
        Resource g = rManager.GetResource("Gold");
        if (r != null)
        {
            if (r.Consume(1))
            {
                g.Add(r.info.marketSellPrice);
                ResourceEyeCandy.BurstEyeCandy(g.info, Input.mousePosition, r.info.marketSellPrice, g.iconTransform);
            }
        }
    }

    public void BuyItem(string itemName)
    {
        Resource r = rManager.GetResource(itemName);
        Resource g = rManager.GetResource("Gold");
        if (r != null)
        {
            if (g.Consume(r.info.marketBuyPrice))
            {
                r.Add(1);
                ResourceEyeCandy.BurstEyeCandy(r.info, Input.mousePosition, 1, r.iconTransform);
            }
        }

    }
}
