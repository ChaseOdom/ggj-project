using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILootSource {
    public int GoldAmount();
    public int GearAmount();
    public int TakeGold(int amount);
    public int TakeGear(int amount);
    }