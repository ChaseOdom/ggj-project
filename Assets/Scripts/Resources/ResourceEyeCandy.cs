using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceEyeCandy : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Image image;

    [Header("Properties")]
    [SerializeField] Vector2 velocityRange;
    [SerializeField] Vector2 initialVelocityTimeRange;
    [SerializeField] Vector2 stoppedTimeRange;
    [SerializeField] Vector2 finalTravelTimeRange;

    public void Initialize(Vector2 position, float rotation, ResourceInfo resource, RectTransform destination)
    {
        image.sprite = resource.resourcePickupSprite;
        image.SetNativeSize();
        transform.position = position;
        transform.rotation = Quaternion.identity;
        transform.Rotate(0, 0, rotation);
        transform.localScale = Vector3.zero;
        StartCoroutine(DoAnimation(Random.Range(velocityRange.x, velocityRange.y), destination, resource.eyeCandyScale, resource.collectSound));
    }

    IEnumerator DoAnimation(float baseVelocity, RectTransform destination, float eyeCandyScale, AUDIO_ID collectSFX)
    {
        float initialTime = Random.Range(initialVelocityTimeRange.x, initialVelocityTimeRange.y);
        float stoppedTime = Random.Range(stoppedTimeRange.x, stoppedTimeRange.y);
        float finalTime = Random.Range(finalTravelTimeRange.x, finalTravelTimeRange.y);
        Vector3 finalScale = new Vector3(eyeCandyScale, eyeCandyScale, eyeCandyScale);
        yield return AnimationUtility.Animation(initialTime, AnimationUtility.exponentialAnimation, (t) =>
        {
            transform.position += transform.right * baseVelocity * Time.unscaledDeltaTime * (1 - t);
            transform.localScale = Vector3.Lerp(Vector3.zero, finalScale, Mathf.Clamp01(t * 3));
        }, unscaledTime: true);
        yield return new WaitForSecondsRealtime(stoppedTime);
        Vector3 initialPos = transform.position;
        yield return AnimationUtility.Animation(finalTime, AnimationUtility.exponentialAnimation, (t) =>
        {
            transform.position = Vector3.Lerp(initialPos, destination.position, t);
        }, unscaledTime: true);
        AudioMaster.PlayGlobal(collectSFX, AUDIO_BUS.SFX);
        Reclaim(this);
    }

    //static/pooling methods
    static Stack<ResourceEyeCandy> pool;

    static void InitializePool()
    {
        if (pool == null)
        {
            pool = new Stack<ResourceEyeCandy>();
            UnityEngine.SceneManagement.SceneManager.activeSceneChanged += (_, __) => pool.Clear();
        }
    }

    static ResourceEyeCandy GetNew()
    {
        InitializePool();

        if (pool.Count > 0)
        {
            var candy = pool.Pop();
            candy.gameObject.SetActive(true);
            return candy;
        }
        else
        {
            return Instantiate(Prefabs.library.resourceEyeCandyPrefab, EyeCandyCanvas.current.transform);
        }
    }

    static void Reclaim(ResourceEyeCandy candy)
    {
        InitializePool();

        candy.StopAllCoroutines();
        candy.gameObject.SetActive(false);
        pool.Push(candy);
    }

    public static void BurstEyeCandy(ResourceInfo resource, Vector2 canvasPosition, int count, RectTransform destination)
    {
        if (count == 0 || !EyeCandyCanvas.current)
            return;

        float currAngle = Random.Range(0, 360);
        float angleOffsets = 360f / count;
        for (int i = 0; i < count; ++i)
        {
            var candy = GetNew();
            candy.Initialize(canvasPosition, currAngle + Random.Range(-angleOffsets / 2f, angleOffsets / 2f), resource, destination);
            currAngle += angleOffsets;
        }
    }
}
