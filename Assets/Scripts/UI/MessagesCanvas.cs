using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessagesCanvas : MonoBehaviour
{
    public static MessagesCanvas current;
    [SerializeField] bool testShowMessage;
    public Canvas canvas;

    private void Awake()
    {
        current = this;
        canvas = GetComponent<Canvas>();
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (testShowMessage && Input.GetMouseButtonDown(0) && !TutorialPopup.messageActive)
        {
            TestMessage(Input.mousePosition);
        }
    }
#endif

    [ContextMenu("Test Error Message")]
    void TestError() => ErrorBanner.ShowMessage("u did a fuky wuky", ErrorBanner.DismissAfterTimeCondition(2f));
    void TestMessage(Vector2 position) => TutorialPopup.ShowMessage("Hello world yaddda yadda yadda this is a message ooga booga", position, TutorialPopup.dismissOnClickCondition);
}
