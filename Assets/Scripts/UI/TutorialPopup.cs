using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPopup : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Animator animator;
    [SerializeField] Text textField;

    [Header("Properties")]
    [SerializeField] float characterAppearDelay;
    [SerializeField] float dismissForcedDelay;

    Coroutine messageRoutine;

    void StartShowMessage(string message, Vector2 location, System.Func<bool> dismissCondition)
    {
        if (location.x < 250f)
            location.x = 250f;
        if (location.x > (MessagesCanvas.current.canvas.pixelRect.width - 250f))
            location.x = MessagesCanvas.current.canvas.pixelRect.width - 250f;
        if (location.y < 100f)
            location.y = 100f;
        if (location.y > (MessagesCanvas.current.canvas.pixelRect.height - 100f))
            location.y = MessagesCanvas.current.canvas.pixelRect.height - 100f;

        textField.text = "";
        transform.position = location;
        if (dismissCondition == null)
            dismissCondition = () => false;
        messageRoutine = StartCoroutine(ShowMessageRoutine(message, dismissCondition));
    }

    void Dismiss()
    {
        if (messageRoutine != null)
        {
            StopCoroutine(messageRoutine);
            messageRoutine = null;
        }

        animator.SetBool("Active", false);
    }

    IEnumerator ShowMessageRoutine(string message, System.Func<bool> dismissCondition)
    {
        animator.SetBool("Active", true);
        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < message.Length; ++i)
        {
            textField.text = message.Substring(0, i);
            yield return new WaitForSeconds(characterAppearDelay);
            if (Input.GetMouseButtonDown(0))
            {
                textField.text = message;
                break;
            }
        }

        yield return new WaitForSeconds(dismissForcedDelay);

        while (!dismissCondition.Invoke())
            yield return null;
        Dismiss();
    }

    static TutorialPopup activePopup;
    static TutorialPopup GetActive()
    {
        if (!activePopup)
            activePopup = Instantiate(Prefabs.library.tutorialPopupPrefab, MessagesCanvas.current.transform);
        return activePopup;
    }

    public static void ShowMessage(string message, Vector2 location, System.Func<bool> dismissCondition = null)
    {
        GetActive().StartShowMessage(message, location, dismissCondition);
    }

    public static void ForceDismissMessage()
    {
        GetActive().Dismiss();
    }

    public static System.Func<bool> dismissOnClickCondition => () => Input.GetMouseButtonDown(0);
    public static bool messageActive => GetActive().messageRoutine != null;
}
