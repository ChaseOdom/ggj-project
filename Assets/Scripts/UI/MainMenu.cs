using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject settingsMenu;
    [SerializeField]
    private GameObject buttons;

    public void PlayButtonClicked()
    {
        print("play clicked");
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainScene");
    }

    public void SettingsButtonClicked()
    {
        buttons.SetActive(settingsMenu.activeInHierarchy);
        settingsMenu.SetActive(!settingsMenu.activeInHierarchy);
    }

    public void ExitButtonClicked()
    {
        Application.Quit();
    }
}
