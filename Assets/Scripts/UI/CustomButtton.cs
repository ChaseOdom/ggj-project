using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;

public class CustomButtton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    bool hovering;
    Button button;
    public bool playUISounds = true;

    private void Awake()
    {
        button = GetComponent<Button>();
        if (button)
        {
            button.onClick.AddListener(buttonClickSound);
        }
    }

    UnityAction buttonClickSound => () =>
    {
        if (playUISounds)
            AudioMaster.PlayGlobal(AUDIO_ID.UI_CLICK, AUDIO_BUS.SFX);
    };

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!hovering)
        {
            MouseCursor.hoverCount++;
            hovering = true;
            if (playUISounds)
                AudioMaster.PlayGlobal(AUDIO_ID.UI_HOVER, AUDIO_BUS.SFX);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (hovering)
        {
            MouseCursor.hoverCount--;
            hovering = false;
        }
    }

    private void OnDisable()
    {
        if (hovering)
        {
            MouseCursor.hoverCount--;
            hovering = false;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!button)
        {
            if (playUISounds)
                AudioMaster.PlayGlobal(AUDIO_ID.UI_CLICK, AUDIO_BUS.SFX);
        }
    }
}
