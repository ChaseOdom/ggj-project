using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [HideInInspector] public bool paused;

    [Header("References")]
    [SerializeField] GameObject pauseMenu;
    [SerializeField] GameObject settingsMenu;
    [SerializeField] GameObject confirmMenu;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.P))
        {
            TogglePause();
        }
    }

    public void TogglePause()
    {
        paused = !paused;
        pauseMenu.SetActive(paused);
        Time.timeScale = paused ? 0 : 1;
        if (paused)
        {
            AudioMaster.OnPause();
        }
        else
        {
            AudioMaster.OnResume();
        }
    }

    public void ToggleSettings()
    {
        settingsMenu.SetActive(!settingsMenu.activeInHierarchy);
    }

    public void ToggleMenuConfirm()
    {
        confirmMenu.SetActive(!confirmMenu.activeInHierarchy);
    }

    public void MenuPressed()
    {
        Time.timeScale = 1f;
        Application.Quit();
        //UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenuScene");
    }
}
