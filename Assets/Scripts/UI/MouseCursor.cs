using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCursor : MonoBehaviour
{
    [SerializeField] UnityEngine.UI.Image cursorImage;
    [SerializeField] Sprite defaultSprite;
    [SerializeField] Sprite hoverSprite;
    [SerializeField] Sprite clickSprite;

    Sprite overrideSprite;

    [System.Serializable] public class MouseSprite
    {
        public string name;
        public Sprite sprite;
    }
    [SerializeField] List<MouseSprite> sprites;
    Dictionary<string, Sprite> overrideSprites;
    static MouseCursor current;
    static int _hoverCount;
    public static int hoverCount
    {
        get => _hoverCount;
        set
        {
            bool stateToggled = value * _hoverCount == 0;
            _hoverCount = value;
            if (stateToggled && current)
                current.FixSprite();
        }
    }

    void FixSprite()
    {
        bool hovering = hoverCount > 0;
        if (hovering)
        {
            if (Input.GetMouseButton(0))
                cursorImage.sprite = clickSprite;
            else
                cursorImage.sprite = hoverSprite;
        }
        else
        {
            if (overrideSprite != null)
                cursorImage.sprite = overrideSprite;
            else
                cursorImage.sprite = defaultSprite;
        }
        cursorImage.rectTransform.pivot = Utility.ProportionalPivot(cursorImage.sprite);
        cursorImage.rectTransform.anchoredPosition = Vector2.zero;
    }

    private void Awake()
    {
        overrideSprites = new Dictionary<string, Sprite>();
        foreach (var sprite in sprites)
        {
            overrideSprites[sprite.name] = sprite.sprite;
        }
        _hoverCount = 0;
        current = this;
        cursorImage.gameObject.SetActive(true);
        FixSprite();
    }

    private void Start()
    {
        Cursor.visible = false;
    }

    private void Update()
    {
        transform.position = Input.mousePosition;
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
            FixSprite();
    }

    public static void SetCursorOverride(string spriteID)
    {
        if (current)
        {
            current.overrideSprite = current.overrideSprites[spriteID];
            current.FixSprite();
        }
    }

    public static void ClearCursorOverride()
    {
        if (current)
        {
            current.overrideSprite = null;
            current.FixSprite();
        }
    }
}
