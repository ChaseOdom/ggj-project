using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyRoomsMenu : MonoBehaviour
{
    [SerializeField]
    private Button[] buyRoomsMenuButtons;
    [SerializeField]
    private PlayerResourceManager playerResourceManager;

    // TODO: balance room costs
    // Indexes 0 - 6 are dinos, 7 = incubator, 8 = trap room, 9 = bell
    public static int[] roomCosts = new int[] { 10, 20, 30, 40, 50, 60, 70, 30, 30, 30 };


    // Start is called before the first frame update
    void Start()
    {
        buyRoomsMenuButtons = new Button[roomCosts.Length];
    }

    // Update is called once per frame
    void Update()
    {
        /*
        for (int i = 0; i < roomCosts.Length; ++i)
        {
            if (playerResourceManager.ResourceCountOfType("Egg") >= roomCosts[i])
            {
                buyRoomsMenuButtons[i].interactable = false;
            }
            else
            {
                buyRoomsMenuButtons[i].interactable = true;
            }
        }
        */
    }

}
