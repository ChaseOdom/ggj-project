using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour
{
    [SerializeField]
    private AUDIO_BUS audioBus;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnVolumeSliderChanged(Slider volumeSlider)
    {
        float volume = volumeSlider.value / volumeSlider.maxValue;
        AudioMaster.SetVolume(audioBus, volume);
    }
}
