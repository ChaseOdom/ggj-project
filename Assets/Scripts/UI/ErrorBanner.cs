using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrorBanner : MonoBehaviour
{
    [Header("References")]
    [SerializeField] Animator animator;
    [SerializeField] Text textField;

    Coroutine checkDismissRoutine;
    bool active;

    void StartShowMessage(string message, System.Func<bool> dismissCondition)
    {
        if (active)
            ForceDismissMessage();

        textField.text = message;
        if (dismissCondition != null)
            checkDismissRoutine = StartCoroutine(CheckDismissCondition(dismissCondition));
        active = true;
        animator.SetBool("Active", true);
    }

    void Dismiss()
    {
        if (checkDismissRoutine != null)
        {
            StopCoroutine(checkDismissRoutine);
            checkDismissRoutine = null;
        }

        active = false;
        animator.SetBool("Active", false);
    }

    IEnumerator CheckDismissCondition( System.Func<bool> dismissCondition)
    {
        while (!dismissCondition.Invoke())
            yield return null;
        Dismiss();
    }

    static ErrorBanner activePopup;
    static ErrorBanner GetActive()
    {
        if (!activePopup)
            activePopup = Instantiate(Prefabs.library.errorBanner, MessagesCanvas.current.transform);
        return activePopup;
    }

    public static void ShowMessage(string message, System.Func<bool> dismissCondition = null)
    {
        GetActive().StartShowMessage(message, dismissCondition);
    }

    public static void ForceDismissMessage()
    {
        GetActive().Dismiss();
    }

    public static System.Func<bool> dismissOnClickCondition => () => Input.GetMouseButtonDown(0);
    public static System.Func<bool> DismissAfterTimeCondition(float delay)
    {
        float currTime = Time.time;
        return () => (Time.time - currTime) >= delay;
    }
    public static bool messageActive => GetActive().active;
}
