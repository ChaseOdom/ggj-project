using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinosaurSpawner : MonoBehaviour
{
    public GameObject dinosaur;
    public bool onCoolDown;
    public float coolDownTimer;
    public int spawnCount;
    public int eggCost;

    // Start is called before the first frame update
    void Start()
    {
        // TODO change to spawn inside room when it's placed
        //SpawnDinosaur();
        onCoolDown = false;
    }

    // TODO add to global dino list and have spawn cooldown timer
    void SpawnDinosaur()
    {
        for (int i = 0; i < spawnCount; ++i)
        {
            var dino = Instantiate(dinosaur, transform.position + new Vector3(1.5f, -1.5f), Quaternion.identity);
            DinosaurDatabase.DinoSpawned(dino.GetComponent<Dinosaur>());
        }
    }

    private void OnMouseDown()
    {
        if (AdventurerSpawner.waveInProgress)
        {
            if (PlayerResourceManager.instance.GetResource("Eggs").Consume(1)) {
                onCoolDown = true;
                StartCoroutine(waitForCooldown());
                SpawnDinosaur();
            }
        }
    }

    IEnumerator waitForCooldown()
    {
        yield return new WaitForSeconds(coolDownTimer);
        onCoolDown = false;
    }
}
