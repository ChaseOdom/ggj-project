using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dinosaur : MonoBehaviour
{
    public float health;
    private float speed;
    private float attack;
    private float hearingRange;
    private float lootCarry;
    private float attackRange;
    public Vector3 currPos => transform.position;
    public bool debugAI;
    public Adventurer adventurerTarget;
    public Room adventurerTargetRoom;
    public DinosaurType dinosaurType;
    public Bell bellTarget;
    public DungeunUtils dungeunUtils => DungeunUtils.instance;
    public Room prevDinoRoom;
    public Room currDinoRoom;
    public DungeonPathfinder dungeonPathfinder;
    public Animator animator;
    bool goingToBell;
    //public HashSet<String, Integer>

    // Start is called before the first frame update
    void Start()
    {
        health = DinosaurDatabase.dinoStatMatrix[(int)dinosaurType, (int)DinosaurStats.HEALTH];
        speed = DinosaurDatabase.dinoStatMatrix[(int)dinosaurType, (int)DinosaurStats.SPEED];
        attack = DinosaurDatabase.dinoStatMatrix[(int)dinosaurType, (int)DinosaurStats.ATTACK];
        hearingRange = DinosaurDatabase.dinoStatMatrix[(int)dinosaurType, (int)DinosaurStats.HEARING_RANGE];
        lootCarry = DinosaurDatabase.dinoStatMatrix[(int)dinosaurType, (int)DinosaurStats.LOOT_CARRY];
        attackRange = DinosaurDatabase.dinoStatMatrix[(int)dinosaurType, (int)DinosaurStats.ATTACK_RANGE];

        currDinoRoom = dungeunUtils.GetRoom(currPos);
        prevDinoRoom = null;
        dungeonPathfinder.speed = speed;
        dungeonPathfinder.destination = GetDest();
    }

    // Update is called once per frame
    void Update()
    {
        currDinoRoom = dungeunUtils.GetRoom(currPos);
        // entered or left a room
        if (currDinoRoom != prevDinoRoom)
        {
            // entered new room
            if (currDinoRoom != null)
            {
                currDinoRoom.DinosaurEntered(this);
            }
            // left a room
            if (prevDinoRoom != null)
            {
                prevDinoRoom.DinosaurExited(this);
            }
            prevDinoRoom = currDinoRoom;
        }

        if (adventurerTarget == null)
        {
            dungeonPathfinder.speed = speed;
            animator.SetBool("Attacking", false);
        }

        // if we're at our destination, see if we can attack, get loot, or find a new one
        if (ReachedDestination())
        {
            if (goingToBell)
                bellTarget = null;
            dungeonPathfinder.destination = GetDest();
            /*
            List<ILootSource> lootSrc = currDinoRoom.LootInRoom();
            if (lootSrc.Count != 0)
            {
                GetLoot(lootSrc);
            }
            */
        }
        if (adventurerTarget != null)
        {
            Attack();
        }
        // check if dead
        Die();
    }

    Vector3 GetDest()
    {
        goingToBell = false;
        // no immediate target and no player directed target
        if (bellTarget == null && adventurerTarget == null)
        {
            adventurerTarget = GetAdventurerInSameRoom();
            if (adventurerTarget != null)
            {
                return adventurerTarget.transform.position;
            }
            // no adventurer in same room, so wander around room
            else
            {
                return FindWanderDest();
            }
        }
        // player rang bell to call dino, not already attacking
        else if (bellTarget != null && adventurerTarget == null)
        {
            goingToBell = true;
            return bellTarget.transform.position;
        }
        // dino's already attacking regardless of whether player rang bell to call dino
        return adventurerTarget.transform.position;
    }

    void Attack()
    {
        if (adventurerTarget.myRoom != currDinoRoom || adventurerTarget.currHP <= 0)
            adventurerTarget = null;

        if (adventurerTarget != null)
        {
            // attack while adventurer is in attack range and alive
            float distToTarget = Vector3.Distance(this.currPos, adventurerTarget.transform.position);
            if (distToTarget <= attackRange)
            {
                dungeonPathfinder.speed = 0f;
                adventurerTarget.currHP -= attack * Time.deltaTime;
                animator.SetBool("Attacking", true);
            }
            else
            {
                dungeonPathfinder.speed = speed;
                animator.SetBool("Attacking", false);
                adventurerTarget = null;
            }
        }
    }

    void Die()
    {
        if (health <= 0)
        {
            if (currDinoRoom != null)
            {
                currDinoRoom.DinosaurExited(this);
            }
            DinosaurDatabase.DinoDied(this);
            Destroy(gameObject);
        }
    }

    /*
    void GetLoot(List<ILootSource> lootSrc)
    {
        for (int i = 0; i < lootSrc.Count; ++i)
        {

        }
    }
    */

    bool ReachedDestination()
    {
        if (Utility.VectorMagnitudeLTE(dungeonPathfinder.destination - currPos, dungeonPathfinder.tolerance))
        {
            return true;
        }
        return false;
    }

    Adventurer GetAdventurerInSameRoom()
    {
        Adventurer target = null;
        // If adventurer is in same room as dino, attack them
        if (currDinoRoom != null)
        {
            List<Adventurer> adventurersInRoom = currDinoRoom.AdventurersInRoom();
            target = Utility.RandomFromList(adventurersInRoom);
        }
        return target;
    }

    Vector3 FindWanderDest()
    {
        var myRoom = this.currDinoRoom;
        if (myRoom == null)
            return transform.position;

        Vector2 offset = Vector2.zero;
        for (int i = 0; i < 10; ++i)
        {
            offset.x = UnityEngine.Random.Range(0.5f, 1.3f) * (UnityEngine.Random.Range(0f, 1f) > 0.5f ? -1 : 1);
            offset.y = UnityEngine.Random.Range(0.5f, 1.3f) * (UnityEngine.Random.Range(0f, 1f) > 0.5f ? -1 : 1);
            var room = DungeunUtils.instance.GetRoom(transform.position + (Vector3)offset);
            if (room == myRoom)
                return transform.position + (Vector3)offset;
        }
        return transform.position;
    }

}
