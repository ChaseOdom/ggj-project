using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuDinoSpawner : MonoBehaviour
{
    public List<GameObject> dinosaurs;
    public DungeunUtils utils;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnDinos());
    }

    IEnumerator SpawnDinos()
    {
        yield return new WaitForSeconds(.1f);
        utils.BakePathfindingGrid();
        foreach (GameObject dino in dinosaurs)
        {
            var spawnedDino = Instantiate(dino, transform.position + new Vector3(1.5f, -1.5f), Quaternion.identity);
            DinosaurDatabase.DinoSpawned(spawnedDino.GetComponent<Dinosaur>());
        }
    }
}
