using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditDungeon : MonoBehaviour
{
    public Dungeon dungeon;
    public DungeunUtils utils;
    public Animator anim;
    public Texture2D wreckingBall;
    public Texture2D wrench;
    public Room compsognathusRoom;
    public Room velociraptorRoom;
    public Room dilophosaurusRoom;
    public Room pterodactylRoom;
    public Room tRexRoom;
    public Room quetzalcoatlusRoom;
    public Room chickenRoom;
    public Room incubatorRoom;
    public Room trapRoom;
    public Room bellRoom;
    public Room entranceRoom;
    public Room lootRoom;
    public Room roomBeingPlaced;
    public SavedDungeonLayout initialDungeon;
    [SerializeField]
    private GameObject buyRoomsMenu;
    public GameObject dinoSpawner;
    public GameObject Border;

    // TODO: balance room costs
    // Indexes 0 - 6 are dinos, 7 = incubator, 8 = trap room, 9 = bell
    public static int[] roomCosts = new int[] { 10, 20, 30, 40, 50, 60, 70, 30, 30, 30 };

    public void MenuClicked()
    {
        anim.SetBool("Open", !anim.GetBool("Open"));
    }

    private void Start()
    {
        LoadDungeon(initialDungeon);
        LoadBorders();
    }

    void LoadBorders()
    {
        Vector2 xBounds = new Vector2(-1, dungeon.gridX);
        Vector2 yBounds = new Vector2(1, dungeon.gridY);
        for(float i = xBounds.x; i <= xBounds.y; i++)
        {
            for(float j = yBounds.x; j >= yBounds.y; j--)
            {
                if(i == xBounds.x || i == xBounds.y || j == yBounds.x || j == yBounds.y)
                {
                    GameObject g = Instantiate<GameObject>(Border);
                    g.transform.position = utils.CoordPosToWorldPos(new Vector2(i, j));
                }
            }
        }
    }

    void LoadDungeon(SavedDungeonLayout layout)
    {
        foreach (var roomInfo in layout.rooms)
        {
            PlaceRoomImmediate(roomInfo.prefab, roomInfo.coords);
        }
        foreach (var corridoords in layout.corridors)
        {
            PlaceCorridorImmediate(corridoords);
        }
    }
    
    void PlaceCorridorImmediate(Vector2 coords)
    {
        Vector2 indices = utils.CoordToGridIndex(coords);
        utils.GetTile((int)indices.x, (int)indices.y).GetComponentInChildren<Background>().FlipWithCorridor();
    }

    void PlaceRoomImmediate(Room prefab, Vector2 coords)
    {
        var room = Instantiate(prefab, DungeunUtils.instance.CoordPosToWorldPos(coords), Quaternion.identity);

        room.cam = Camera.main;
        dungeon.rooms.Add(room);
        room.dungeon = dungeon;
        room.coord = coords;
        room.SetTileCoords();
        room.DisableTiles();
        room.ChangeGridTilesToRoomTiles();
        utils.CheckForCorridors();

        foreach (BoxCollider2D bc in GetComponentsInChildren<BoxCollider2D>())
        {
            bc.enabled = true;
        }
    }

    public bool AttemptFinalization()
    {
        if (new List<Room>(utils.GetRoomsOfType(RoomType.ENTRANCE)).Count == 0)
        {
            ErrorBanner.ShowMessage("At least one entrance room required in the dungeon!", ErrorBanner.DismissAfterTimeCondition(5));
            return false;
        }
        if (new List<Room>(utils.GetRoomsOfType(RoomType.LOOT)).Count == 0)
        {
            ErrorBanner.ShowMessage("At least one loot room required in the dungeon!", ErrorBanner.DismissAfterTimeCondition(5));
            return false;
        }
        if (!utils.BakePathfindingGrid())
        {
            ErrorBanner.ShowMessage("Make sure all rooms are connected with doors!", ErrorBanner.DismissAfterTimeCondition(5));
            return false;
        }
        return true;
    }

    public void OnFinalizePressed()
    {
        if (AttemptFinalization())
        {
            GameManager.ChangeGameState(GameState.DUNGEON_DEFENSE);
        }
    }

    public void DisableAllModes()
    {
        dungeon.destroyRoomMode = false;
        dungeon.editMode = false;
        dungeon.placingRoom = false;
        foreach (Tile t in utils.AllTiles())
        {
            if (t.isBackground || t.isCorridor)
            {
                t.GetComponentInChildren<BoxCollider2D>().enabled = true;
            }
        }
        if (roomBeingPlaced)
        {
            Destroy(roomBeingPlaced);
        }
        buyRoomsMenu.SetActive(false);
        MouseCursor.ClearCursorOverride();
    }

    public void EditModeSwitch()
    {
        bool newEditMode = !dungeon.editMode;
        DisableAllModes();
        dungeon.editMode = newEditMode;
        
        if (dungeon.editMode)
        {
            MouseCursor.SetCursorOverride("Wrench");
        }
    }

    public void BuyRoom()
    {
        /*
        
        */
        buyRoomsMenu.SetActive(!buyRoomsMenu.activeInHierarchy);
    }

    public void DestroyRoomMode()
    {
        bool newDestroyRoomMode = !dungeon.destroyRoomMode;
        DisableAllModes();
        dungeon.destroyRoomMode = newDestroyRoomMode;
        if (dungeon.destroyRoomMode)
        {
            MouseCursor.SetCursorOverride("Wrecking Ball");
        }
    }

    public void CompsognathusClicked()
    {
        if (PlayerResourceManager.instance.GetResource("Gold").Consume(10))
            PlaceRoom(compsognathusRoom);
        else
            ErrorBanner.ShowMessage("Need more gold!");
    }

    public void VelociraptorClicked()
    {
        if (PlayerResourceManager.instance.GetResource("Gold").Consume(15))
            PlaceRoom(velociraptorRoom);
        else
            ErrorBanner.ShowMessage("Need more gold!");
    }

    public void DilophosaurusClicked()
    {
        if (PlayerResourceManager.instance.GetResource("Gold").Consume(30))
            PlaceRoom(dilophosaurusRoom);
        else
            ErrorBanner.ShowMessage("Need more gold!");
    }
    public void LootRoomClicked()
    {
        PlaceRoom(lootRoom);
    }
    public void EntranceRoomClicked()
    {
        PlaceRoom(entranceRoom);
    }

    public void PterodactylClicked()
    {
        if (PlayerResourceManager.instance.GetResource("Gold").Consume(20))
            PlaceRoom(pterodactylRoom);
        else
            ErrorBanner.ShowMessage("Need more gold!");
    }

    public void TRexClicked()
    {
        if (PlayerResourceManager.instance.GetResource("Gold").Consume(50))
            PlaceRoom(tRexRoom);
        else
            ErrorBanner.ShowMessage("Need more gold!");
    }

    public void QuetzalcoatlusClicked()
    {
        Debug.Log("QuetzalcoatlusClicked");
        PlaceRoom(quetzalcoatlusRoom);
    }

    public void ChickenClicked()
    {
        Debug.Log("ChickenClicked");
        PlaceRoom(chickenRoom);
    }

    public void IncubatorClicked()
    {
        Debug.Log("IncubatorClicked");
        PlaceRoom(incubatorRoom);
    }

    public void TrapClicked()
    {
        if (PlayerResourceManager.instance.GetResource("Gear").Consume(3))
            PlaceRoom(trapRoom);
        else
            ErrorBanner.ShowMessage("Need more adventurer gear!");
    }

    public void BellClicked()
    {
        if (PlayerResourceManager.instance.GetResource("Gear").Consume(2))
            PlaceRoom(bellRoom);
        else
            ErrorBanner.ShowMessage("Need more adventurer gear!");
    }

    // return to dungeon editing menu
    public void GoBackButtonClicked()
    {
        buyRoomsMenu.SetActive(!buyRoomsMenu.activeInHierarchy);
    }

    public void PlaceRoom(Room room)
    {
        bool newPlacingRoom = !dungeon.placingRoom;
        DisableAllModes();
        dungeon.placingRoom = newPlacingRoom;
        if (dungeon.placingRoom)
        {

            foreach (Room r in utils.GetRooms())
            {
                foreach (BoxCollider2D bc in r.GetComponentsInChildren<BoxCollider2D>())
                {
                    bc.enabled = false;
                }
            }
            foreach (Tile t in utils.AllTiles())
            {
                if (t.isBackground || t.isCorridor)
                {
                    t.GetComponentInChildren<BoxCollider2D>().enabled = false;
                }
            }
            roomBeingPlaced = Instantiate(room);
            roomBeingPlaced.dungeon = dungeon;
            roomBeingPlaced.gameObject.AddComponent<PlaceRoom>();
            
            foreach (BoxCollider2D bc in roomBeingPlaced.GetComponentsInChildren<BoxCollider2D>())
            {
                bc.enabled = false;
            }
            roomBeingPlaced.GetComponent<BoxCollider2D>().enabled = true;
        }
    }

}
