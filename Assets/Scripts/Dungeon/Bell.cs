using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bell : MonoBehaviour
{
    public Vector3 position;
    public HashSet<Dinosaur> dinosaursSpawned => DinosaurDatabase.dinosaursSpawned;
    public Slider progressBar;

    private float coolDownPeriodInSeconds = 10;
    private float nextRing;     // earliest time bell can be rung again
    private SpriteRenderer spriteRenderer;

    void Awake()
    {
        nextRing = Time.time;
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        progressBar.value = 1f;
    }

    public void BellPressed()
    {
        if (Time.time >= nextRing && GameManager.currGameState == GameState.DUNGEON_DEFENSE) {
            nextRing = Time.time + coolDownPeriodInSeconds;
            GetDinosaurs();
            CoolDown();
        }
    }

    void GetDinosaurs()
    {
        foreach (Dinosaur dino in dinosaursSpawned)
        {
            float dist = Vector3.Distance(this.position, dino.currPos);
            if (dist <= DinosaurDatabase.dinoStatMatrix[(int)dino.dinosaurType, (int)DinosaurStats.HEARING_RANGE])
            {
                dino.bellTarget = this;
            }
        }
    }

    public void CoolDown()
    {
        StartCoroutine(FillCoolDownBar());
    }

    IEnumerator FillCoolDownBar()
    {
        // fill progress bar
        for (float t = 0; t < 1.0f; t += Time.deltaTime / coolDownPeriodInSeconds)
        {
            progressBar.value = t;
            if (t >= 1.0f)
            {
                t = 1.0f;
            }
            yield return null;
        }
    }

}
