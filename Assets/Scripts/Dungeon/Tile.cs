using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public bool isBackground = true;
    public bool isCorridor = false;
    public bool isRoom = false;
    public bool isDoor = false;
    public GameObject tileAsset;
    public Vector2 coord;
    public Room room => DungeunUtils.instance.GetRoom((int)coord.x, -(int)coord.y);
    public Vector3 worldCenter => DungeunUtils.instance.TileCenter(this);

    public void ResetTile()
    {
        isBackground = true;
        isCorridor = false;
        isRoom = false;
        isDoor = false;
    }
}
