using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeRoom : MonoBehaviour
{
    Dungeon dungeon;
    public Sprite spikeUp;
    public Sprite spikeDown;
    public int damage = 10;
    SpriteRenderer spRend;
    bool spikesUp = false;

    private void Start()
    {
        dungeon = FindObjectOfType<Dungeon>();
        spRend = GetComponent<SpriteRenderer>();
    }

    public void SpikesPressed()
    {
        print("spikeroom pressed");
        if (dungeon.playing && !spikesUp)
        {
            spikesUp = false;
            foreach(Adventurer a in GetComponent<Tile>().room.AdventurersInRoom())
            {
                a.currHP -= damage;
            }
            spRend.sprite = spikeUp;
            StartCoroutine(RetractSpikes());
        }
    }

    IEnumerator RetractSpikes()
    {
        yield return new WaitForSeconds(1f);
        spRend.sprite = spikeDown;
        yield return new WaitForSeconds(1f);
        spikesUp = false;
    }
}
