using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeunUtils : MonoBehaviour
{
    public Dungeon dungeon;
    public Camera cam;
    public static DungeunUtils instance;
    Dictionary<Tile, Dictionary<Tile, Tile>> pathfindingInfo;
    public bool pathfindingBaked
    {
        get;
        private set;
    }

    private void Awake()
    {
        instance = this;
    }

    public void FindCorridorSprites()
    {
        foreach (var corridor in CorridorTiles())
        {
            corridor.GetComponentInChildren<Corridor>().FindCorridorSprite();
        }
    }

    public List<Room> GetRooms()
    {
        return dungeon.rooms;
    }

    public Tile[,] GetGrid()
    {
        return dungeon.grid;
    }

    public Tile GetTile(int x, int y)
    {
        if (x < 0 || y < 0 || x >= dungeon.gridX || y >= -dungeon.gridY)
            return null;
        return dungeon.grid[x, y];
    }

    public Tile GetTile(Vector3 worldPosition)
    {
        Vector2 index = CoordToGridIndex(WorldPosToCoordPos(worldPosition));
        return GetTile((int)index.x, (int)index.y);
    }

    public Room GetRoom(Vector3 worldPosition)
    {
        Vector2 index = CoordToGridIndex(WorldPosToCoordPos(worldPosition));
        return GetRoom((int)index.x, (int)index.y);
    }

    public Room GetRoom(int x, int y)
    {
        var tile = GetTile(x, y);
        if (tile == null)
            return null;
        if (!tile.isRoom)
            return null;
        return tile.transform.parent.GetComponent<Room>();
    }

    public IEnumerable<Tile> AllTiles()
    {
        for (int i = 0; i < dungeon.gridX; i++)
        {
            for (int j = 0; j < -dungeon.gridY; j++)
            {
                yield return dungeon.grid[i,j];
            }
        }
    }
    public Tile AnyValidTile()
    {
        for (int i = 0; i < dungeon.gridX; i++)
        {
            for (int j = 0; j < -dungeon.gridY; j++)
            {
                if (!dungeon.grid[i, j].isBackground)
                    return dungeon.grid[i, j];
            }
        }
        return null;
    }

    public Tile GetNextTile(Tile current, Tile desired)
    {
        if (!pathfindingBaked || current.isBackground || desired.isBackground)
            return null;
        return pathfindingInfo[current][desired];
    }
    public IEnumerable<Tile> ValidTiles()
    {
        foreach (var tile in AllTiles())
        {
            if (!tile.isBackground)
                yield return tile;
        }
    }
    public IEnumerable<Tile> CorridorTiles()
    {
        foreach (var tile in AllTiles())
        {
            if (tile.isCorridor)
                yield return tile;
        }
    }

    public IEnumerable<Room> GetRoomsOfType(RoomType type)
    {
        foreach (var room in GetRooms())
        {
            if (room.type == type)
                yield return room;
        }
    }

    public Vector2 GridIndexToCoord(Vector2 gridIndex)
    {
        return CoordToGridIndex(gridIndex);
    }

    public Vector2 CoordToGridIndex(Vector2 coord)
    {
        return new Vector2(coord.x, -1 * coord.y);
    }

    [ContextMenu("Rebake pathfinding grid")]
    public bool BakePathfindingGrid()
    {
        pathfindingBaked = false;
        Dictionary<Tile, bool> visited = new Dictionary<Tile, bool>();
        if (pathfindingInfo != null)
            pathfindingInfo.Clear();
        else
            pathfindingInfo = new Dictionary<Tile, Dictionary<Tile, Tile>>();

        foreach (var tile in ValidTiles())
        {
            pathfindingInfo[tile] = new Dictionary<Tile, Tile>();
            foreach (var destTile in ValidTiles())
            {
                if (destTile != tile)
                    pathfindingInfo[tile][destTile] = null;
            }
            visited[tile] = false;
        }

        Queue<Tile> tileQueue = new Queue<Tile>();
        foreach (var tile in ValidTiles())
        {
            if (!FindShortestPaths(visited, tile, tileQueue))
            {
                Debug.Log("Invalid setup");
                return false;
            }
        }
        pathfindingBaked = true;
        return true;
    }

    bool FindShortestPaths(Dictionary<Tile, bool> visited, Tile startingTile, Queue<Tile> tileQueue)
    {
        tileQueue.Clear();
        tileQueue.Enqueue(startingTile);
        foreach (var tile in ValidTiles())
        {
            visited[tile] = false;
        }
        visited[startingTile] = true;

        while (tileQueue.Count > 0)
        {
            var tile = tileQueue.Dequeue();
            var tileRoom = GetRoom((int)tile.coord.x, -(int)tile.coord.y);
            foreach (Tile neighbour in GetLegalAdjacentTiles(tile))
            {
                if (!visited[neighbour])
                {
                    var neighbourRoom = GetRoom((int)neighbour.coord.x, -(int)neighbour.coord.y);
                    if (tileRoom == neighbourRoom
                        || (tile.isRoom && tile.isDoor && neighbour.isCorridor)
                        || (neighbour.isRoom && neighbour.isDoor && tile.isCorridor)
                        || (tile.isDoor && neighbour.isDoor))
                    {
                        visited[neighbour] = true;
                        tileQueue.Enqueue(neighbour);
                        pathfindingInfo[neighbour][startingTile] = tile;
                    }
                }
            }
        }

        foreach (var tile in visited.Keys)
        {
            if (!visited[tile])
            {
                    return false;
            }
        }
        return true;
    }

    public IEnumerable<Tile> GetAdjacentTiles(Tile t)
    {
        Vector2 coords = CoordToGridIndex(t.coord);
        //left
        coords.x -= 1;
        if (IndicesInBounds(coords))
            yield return GetTile((int)coords.x, (int)coords.y);
        //right
        coords.x += 2;
        if (IndicesInBounds(coords))
            yield return GetTile((int)coords.x, (int)coords.y);
        //up
        coords.x -= 1;
        coords.y -= 1;
        if (IndicesInBounds(coords))
            yield return GetTile((int)coords.x, (int)coords.y);
        //down
        coords.y += 2;
        if (IndicesInBounds(coords))
            yield return GetTile((int)coords.x, (int)coords.y);
    }

    public IEnumerable<Tile> GetLegalAdjacentTiles(Tile t)
    {
        foreach (var tile in GetAdjacentTiles(t))
        {
            if (!tile.isBackground)
                yield return tile;
        }
    }

    public bool CoordsInBounds(Vector2 coord)
    {
        return coord.x >= 0 && coord.x < dungeon.gridX && coord.y <= 0 && coord.y > dungeon.gridY;
    }
    public bool IndicesInBounds(Vector2 indices)
    {
        return CoordsInBounds(GridIndexToCoord(indices));
    }


    public Vector2 WorldPosToCoordPos(Vector2 worldPos)
    {
        Vector2 upperLeft = (Vector2)dungeon.transform.position - new Vector2(dungeon.gridX / 2f, dungeon.gridY / 2f);
        Vector2 rawOffset = worldPos - upperLeft;
        return new Vector2((int)(rawOffset.x), (int)(rawOffset.y));
        /*
        Vector2 result = new Vector2(0, 0);
        result.x = (float)(int)worldPos.x;
        result.y = (float)(int)worldPos.y;

        Vector2 upperLeftScreen = cam.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));
        upperLeftScreen.x = (float)(int)upperLeftScreen.x;
        upperLeftScreen.y = (float)(int)upperLeftScreen.y;

        result.x -= upperLeftScreen.x;
        result.y -= upperLeftScreen.y;
        return result;
        */
    }

    public Vector2 CoordPosToWorldPos(Vector2 gridPos)
    {
        Vector2 upperLeft = (Vector2)dungeon.transform.position - new Vector2(dungeon.gridX / 2f, dungeon.gridY / 2f);
        return upperLeft + new Vector2(gridPos.x, gridPos.y);
        /*
        Vector2 result = gridPos;

        Vector2 upperLeftScreen = cam.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));
        upperLeftScreen.x = (float)(int)upperLeftScreen.x;
        upperLeftScreen.y = (float)(int)upperLeftScreen.y;

        result += upperLeftScreen;
        return result;
        */
    }

    public List<Tile> FindConnectedDoorways(Room room)
    {
        List<Tile> results = new List<Tile>();

        foreach (var tile in room.tiles)
        {
            if (tile.isDoor)
                FindConnectedDoorwaysFromTile(tile, room, results);
        }

        return results;
    }

    void FindConnectedDoorwaysFromTile(Tile tile, Room room, List<Tile> list)
    {
        Queue<Tile> queue = new Queue<Tile>();
        HashSet<Tile> visited = new HashSet<Tile>();
        queue.Enqueue(tile);
        visited.Add(tile);
        while (queue.Count > 0)
        {
            var current = queue.Dequeue();
            foreach (var neighbour in GetLegalAdjacentTiles(current))
            {
                if (!visited.Contains(neighbour))
                {
                    visited.Add(neighbour);
                    if (neighbour.isCorridor)
                    {
                        queue.Enqueue(neighbour);
                    }
                    else if (neighbour.isRoom)
                    {
                        if (GetRoom((int)neighbour.coord.x, -(int)neighbour.coord.y) != room)
                        {
                            if (!list.Contains(neighbour))
                                list.Add(neighbour);
                        }
                    }
                }
            }
        }
    }

    public Vector3 TileCenter(Tile t)
    {
        if (t.isRoom)
            return t.transform.position;
        else
            return t.transform.position + new Vector3(0.5f, -0.5f);
    }

    public void CheckForCorridors()
    {
        foreach (Room r in GetRooms())
        {
            r.CheckForCorridors();
        }
    }

    public int DistanceBetweenTiles(Tile start, Tile end)
    {
        if (start.isBackground || end.isBackground)
            return int.MaxValue;

        int dist = 0;
        while (start != end)
        {
            start = GetNextTile(start, end);
            ++dist;
        }

        return dist;
    }
}
