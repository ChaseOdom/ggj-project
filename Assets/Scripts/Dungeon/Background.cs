using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    Dungeon dungeon;
    DungeunUtils utils => DungeunUtils.instance;
    public GameObject corridor;

    private void Start()
    {
        dungeon = FindObjectOfType<Dungeon>();

        /*
        foreach (Room r in utils.GetRooms())
        {
            r.CheckForCorridors();
        }*/
    }

    private void OnMouseEnter()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;
        if (Input.GetMouseButton(0) && dungeon.drawingCorridors && dungeon.editMode)
        {
            FlipWithCorridor();
        }
    }

    private void OnMouseDown()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;
        if (dungeon.editMode) {
            dungeon.drawingCorridors = true;
            FlipWithCorridor();
        }
    }

    public void FlipWithCorridor()
    {
        
        if (dungeon == null)
            dungeon = FindObjectOfType<Dungeon>();

        Vector2 coord = GetComponentInParent<Tile>().coord;
        GameObject g = Instantiate<GameObject>(corridor, transform.parent);
        Tile t = g.GetComponentInParent<Tile>();
        t.ResetTile();
        t.tileAsset = g;
        t.isCorridor = true;
        t.isBackground = false;
        g.GetComponent<Corridor>().tile = t;

        Tile[,] tiles = dungeon.grid;
        for(int i = 0; i < dungeon.gridX; i++)
        {
            for(int j = 0; j < -1*dungeon.gridY; j++)
            {
                Tile c = tiles[i, j];
                if (c.isCorridor)
                {
                    Corridor cor = c.GetComponentInChildren<Corridor>();
                    utils.FindCorridorSprites();
                }
            }
        }

        Destroy(gameObject);
    }
}
