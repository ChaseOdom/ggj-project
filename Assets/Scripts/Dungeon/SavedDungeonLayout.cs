using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inital Dungeon Layout")]
public class SavedDungeonLayout : ScriptableObject
{
    [System.Serializable]
    public class RoomInfo
    {
        public Room prefab;
        public Vector2 coords;
    }
    public RoomInfo[] rooms;
    public Vector2[] corridors;

    public Coords[] NewCorridors;

    //dimensions of initial grid
    public int xSize;
    public int ySize;
}
