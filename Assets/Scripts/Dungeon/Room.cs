using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RoomType
{
    SPAWNER,
    INCUBATOR,
    LOOT,
    TRAP,
    ENTRANCE
}

public class Room : MonoBehaviour
{
    public Camera cam;
    public Vector2 coord;
    public Vector2 dimensions;
    public Dungeon dungeon;
    public RoomType type;
    Vector2 position = new Vector2(0, 0);
    Vector2 originalPos;
    bool dragging = false;
    bool originalPosReset = false;
    Vector2 originalTransform;
    public List<Tile> tiles;
    DungeunUtils utils => DungeunUtils.instance;

    HashSet<Adventurer> adventurers;
    HashSet<Dinosaur> dinosaurs;
    HashSet<ILootSource> loot;

    public void AddLoot(ILootSource drop)
    {
        loot.Add(drop);
    }
    public void RemoveLoot(ILootSource drop)
    {
        loot.Remove(drop);
    }
    public List<ILootSource> LootInRoom()
    {
        return new List<ILootSource>(loot).FindAll((e) => e.GoldAmount() > 0 || e.GearAmount() > 0);
    }
    public void AdventurerEntered(Adventurer adventurer)
    {
        if (!adventurers.Contains(adventurer))
            adventurers.Add(adventurer);
    }
    public void AdventurerExited(Adventurer adventurer)
    {
        if (adventurers.Contains(adventurer))
            adventurers.Remove(adventurer);
    }
    public List<Adventurer> AdventurersInRoom()
    {
        return new List<Adventurer>(adventurers);
    }
    public void DinosaurEntered(Dinosaur dinosaur)
    {
        if (!dinosaurs.Contains(dinosaur))
            dinosaurs.Add(dinosaur);
    }
    public void DinosaurExited(Dinosaur dinosaur)
    {
        if (dinosaurs.Contains(dinosaur))
            dinosaurs.Remove(dinosaur);
    }
    public List<Dinosaur> DinosaursInRoom()
    {
        return new List<Dinosaur>(dinosaurs);
    }

    private void Awake()
    {
        adventurers = new HashSet<Adventurer>();
        dinosaurs = new HashSet<Dinosaur>();
        loot = new HashSet<ILootSource>();
    }

    public List<Vector2> GetGridCoords()
    {
        List<Vector2> result = new List<Vector2>();
        for(int i = 0; i < (int)dimensions.x; i++)
        {
            for(int j = 0; j < (int)dimensions.y; j++)
            {
                result.Add(new Vector2((float)coord.x + i, (float)coord.y - j));
            }
        }
        return result;
    }

    public void CheckForCorridors()
    {
        foreach(Tile t in tiles)
        {
            t.GetComponent<RoomTile>().CheckForCorridors();
        }
    }

    public List<Tile> GetTiles()
    {
        return tiles;
    }

    public void SetTileCoords()
    {
        foreach(Tile t in tiles)
        {
            RoomTile rTile = t.GetComponent<RoomTile>();
            t.coord = coord + rTile.LocalCoord;
        }
    }

    private void OnMouseDown()
    {
        if (dungeon.editMode)
        {
            ChangeRoomTilesToGridTiles();
            originalPosReset = false;
            dragging = true;
            dungeon.draggingRoom = true;
        }
        else if (dungeon.destroyRoomMode)
        {
            DestroyRoom();
        }
    }

    private void OnMouseUp()
    {
        if (dungeon.editMode)
        {
            dragging = false;
            EnableTiles();
            coord = new Vector2(transform.position.x + dungeon.offsetX, transform.position.y + dungeon.offsetY);
            SetTileCoords();
            DisableTiles();
            ChangeGridTilesToRoomTiles();
            utils.CheckForCorridors();
            dungeon.draggingRoom = false;
            utils.FindCorridorSprites();
        }
    }

    private void OnMouseDrag()
    {
        if (dragging && dungeon.editMode)
        {
            Vector2 pos = cam.ScreenToWorldPoint(Input.mousePosition);

            if (!originalPosReset)
            {
                originalPosReset = true;
                originalPos = pos;
                originalTransform = transform.position;
            }


            Vector2 diff = pos - originalPos;
            diff.x = (float)(int)diff.x;
            diff.y = (float)(int)diff.y;

            Vector2 newCoord = originalTransform + diff;

            //can't move over other rooms
            bool okayMovement = true;
            
            for (int i = 0; i < (int)dimensions.x; i++)
            {
                for(int j = 0; j < (int)dimensions.y; j++)
                {
                    Vector2 c = utils.WorldPosToCoordPos(new Vector2((int)newCoord.x + i, ((int)newCoord.y - j)));
                    if (c.x < 0 || c.x >= dungeon.gridX || c.y > 0 || c.y <= dungeon.gridY)
                    {
                        okayMovement = false;
                    }
                    else
                    {
                        Tile t = utils.GetTile((int)c.x, (int)(-1*c.y));
                        if (t.isRoom)
                        {
                            bool found = false;
                            foreach (Tile a in tiles)
                            {
                                if (a == t)
                                {
                                    found = true;
                                }
                            }
                            okayMovement = found && okayMovement;
                        }
                    }
                }
            }

            if (okayMovement)
            {
                transform.position = newCoord;
                utils.FindCorridorSprites();
            }
        }
    }

    //disables collider on tiles under this room
    public void DisableTiles()
    {
        for(int i = 0; i < (int)dimensions.x; i++)
        {
            for(int j = 0; j < (int)dimensions.y; j++)
            {
                dungeon.grid[i + (int)coord.x, j + -1*(int)coord.y].GetComponentInChildren<BoxCollider2D>().enabled = false;
            }
        }
    }

    public void ChangeGridTilesToRoomTiles()
    {
        foreach(Tile t in tiles)
        {
            Destroy(dungeon.grid[(int)t.coord.x, -1 * (int)t.coord.y].gameObject);
            dungeon.grid[(int)t.coord.x, -1 * (int)t.coord.y] = t;
        }
    }

    public void EnableTiles()
    {
        for (int i = 0; i < (int)dimensions.x; i++)
        {
            for (int j = 0; j < (int)dimensions.y; j++)
            {
                dungeon.grid[i + (int)coord.x, j + -1 * (int)coord.y].GetComponentInChildren<BoxCollider2D>().enabled = true;
            }
        }
    }

    public void ChangeRoomTilesToGridTiles()
    {
        foreach (Tile t in tiles)
        {
            GameObject g = Instantiate<GameObject>(dungeon.backgroundTile, dungeon.transform);
            g.AddComponent<Tile>();
            Tile x = g.GetComponent<Tile>();
            x.tileAsset = g;
            x.coord = t.coord;
            g.transform.position = utils.CoordPosToWorldPos(x.coord);
            dungeon.grid[(int)t.coord.x, -1 * (int)t.coord.y] = x;
        }
    }

    public void DestroyRoom()
    {
        ChangeRoomTilesToGridTiles();
        EnableTiles();
        dungeon.rooms.Remove(this);
        utils.FindCorridorSprites();
        Destroy(gameObject);
    }
}
