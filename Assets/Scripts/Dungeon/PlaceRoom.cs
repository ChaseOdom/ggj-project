using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaceRoom : MonoBehaviour
{
    Camera cam;
    Dungeon dungeon;
    DungeunUtils utils;

    private void Start()
    {
        cam = GameObject.FindObjectOfType<Camera>();
        dungeon = GameObject.FindObjectOfType<Dungeon>();
        utils = GameObject.FindObjectOfType<DungeunUtils>();
    }

    private void FixedUpdate()
    {
        if (cam)
        {
            Vector2 position = cam.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector2(((float)(int)position.x) - 1, ((float)(int)position.y) + 1);
        }
    }

    private void OnMouseDown()
    {
        if (isLegal())
        {
            dungeon.placingRoom = false;

            Room r = GetComponent<Room>();
            r.cam = cam;
            dungeon.rooms.Add(r);
            r.coord = new Vector2(transform.position.x + dungeon.offsetX, transform.position.y + dungeon.offsetY);
            r.SetTileCoords();
            r.DisableTiles();
            r.ChangeGridTilesToRoomTiles();
            utils.CheckForCorridors();
            
            GameObject.FindObjectOfType<EditDungeon>().roomBeingPlaced = null;

            foreach (Tile t in utils.AllTiles())
            {
                if (t.isBackground || t.isCorridor)
                {
                    t.GetComponentInChildren<BoxCollider2D>().enabled = true;
                }
            }
            foreach (Room room in utils.GetRooms())
            {
                var collider = room.GetComponent<BoxCollider2D>();
                if (collider)
                    collider.enabled = true;
                else
                    room.GetComponentInChildren<BoxCollider2D>().enabled = true;
            }
            utils.FindCorridorSprites();

            Destroy(this);
        }
        else
        {
            /*
            GameObject.Find("placement warning").GetComponent<Image>().enabled = true;
            GameObject.Find("placement warning").GetComponentInChildren<Text>().enabled = true;
            */
            if (NotOutOfBounds())
            {
                ErrorBanner.ShowMessage("Placing Room on Room");
            }
            else
            {
                ErrorBanner.ShowMessage("Placing Room Outside grid");
            }
            StartCoroutine(CloseWarning());
        }
    }

    IEnumerator CloseWarning()
    {
        yield return new WaitForSeconds(.75f);
        ErrorBanner.ForceDismissMessage();
        /*
        GameObject.Find("placement warning").GetComponent<Image>().enabled = false;
        GameObject.Find("placement warning").GetComponentInChildren<Text>().enabled = false;*/
    }

    public bool isLegal()
    {
        return NotOutOfBounds() && NotOverRoom();
    }

    public bool NotOverRoom()
    {
        Room r = GetComponent<Room>();
        int currX = (int)transform.position.x + dungeon.offsetX;
        int currY = (int)transform.position.y + dungeon.offsetY;

        int minX = currX;
        int maxX = currX + ((int)r.dimensions.x - 1);
        int minY = currY;
        int maxY = currY - ((int)r.dimensions.y - 1);

        for(int i = minX; i <= maxX; i++)
        {
            for(int j = minY; j >= maxY; j--)
            {
                if (dungeon.grid[i, -1*j].isRoom)
                {
                    return false;
                }
            }
        }

        return true;
    }

    public bool NotOutOfBounds()
    {
        Room r = GetComponent<Room>();
        int currX = (int)transform.position.x + dungeon.offsetX;
        int currY = (int)transform.position.y + dungeon.offsetY;

        int minX = currX;
        int maxX = currX + ((int)r.dimensions.x - 1);
        int minY = currY;
        int maxY = currY - ((int)r.dimensions.y - 1);

        
        return minX >= 0 && minY <= 0 && maxX < dungeon.gridX && maxY > dungeon.gridY;
    }
}
