using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BellHelper : MonoBehaviour
{
    public Bell InnerBell;

    private void OnMouseDown()
    {
        if (GameManager.currGameState == GameState.DUNGEON_DEFENSE)
        {
            InnerBell.BellPressed();
        }
    }
}
