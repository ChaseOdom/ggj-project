using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corridor : MonoBehaviour
{
    Dungeon dungeon;
    DungeunUtils utils;
    public GameObject background;
    public Tile tile;
    public Sprite[] corridorSprites;
    public Vector2[] scale;
    public Vector2[] bcScale;
    public SpriteRenderer spRend;
    public BoxCollider2D bc;

    private void Start()
    {
        dungeon = FindObjectOfType<Dungeon>();
        utils = FindObjectOfType<DungeunUtils>();

        foreach (Room r in utils.GetRooms())
        {
            r.CheckForCorridors();
        }
    }

    private void OnMouseEnter()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;
        if (Input.GetMouseButton(0) && dungeon.drawingBackgrounds && dungeon.editMode)
        {
            FlipWithBackground();
        }
    }

    private void OnMouseDown()
    {
        if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            return;
        if (dungeon.editMode)
        {
            dungeon.drawingBackgrounds = true;
            FlipWithBackground();
        }
    }

    private void FlipWithBackground()
    {
        Vector2 coord = GetComponentInParent<Tile>().coord;
        GameObject g = Instantiate<GameObject>(background, transform.parent);
        Tile t = g.GetComponentInParent<Tile>();
        t.ResetTile();
        t.tileAsset = g;
        t.isCorridor = false;
        t.isBackground = true;
        utils.FindCorridorSprites();
        Destroy(gameObject);
    }

    public Tile[,] GetAdjacentTiles()
    {
        utils = FindObjectOfType<DungeunUtils>();
        Tile[,] result = new Tile[3, 3];
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                result[i + 1, j + 1] = utils.GetTile(i + (int)tile.coord.x, ((int)tile.coord.y) + -j);
            }
        }

        return result;
    }

    public bool CanConnect(Tile t)
    {
        if (t)
        {
            return t.isDoor || t.isCorridor;
        }
        else
        {
            return false;
        }
    }

    public bool[,] AdjacentConnectors()
    {
        utils = FindObjectOfType<DungeunUtils>();
        bool[,] result = new bool[3, 3];
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                if (!(i == 0 && j == 0))
                {
                    Tile t = utils.GetTile(i + (int)tile.coord.x, -1*((int)tile.coord.y) + j);
                    result[i + 1, j + 1] = CanConnect(t);
                }
                else
                {
                    result[i + 1, j + 1] = false;
                }
            }
        }

        return result;
    }

    public void FindCorridorSprite()
    {
        bool[,] adjacents = AdjacentConnectors();

        int index = -1;
        //top, bottom, left, right
        //2 way connections
        //straight
        if(adjacents[1,0] && adjacents[1,2] && !adjacents[0,1] && !adjacents[2, 1])
        {
            index = 0;
        }
        else if (!adjacents[1, 0] && !adjacents[1, 2] && adjacents[0, 1] && adjacents[2, 1])
        {
            index = 1;
        }
        //90 degrees
        else if (adjacents[1, 0] && !adjacents[1, 2] && !adjacents[0, 1] && adjacents[2, 1])
        {
            index = 2;
        }
        else if (adjacents[1, 0] && !adjacents[1, 2] && adjacents[0, 1] && !adjacents[2, 1])
        {
            index = 3;
        }
        else if (!adjacents[1, 0] && adjacents[1, 2] && !adjacents[0, 1] && adjacents[2, 1])
        {
            index = 4;
        }
        else if (!adjacents[1, 0] && adjacents[1, 2] && adjacents[0, 1] && !adjacents[2, 1])
        {
            index = 5;
        }
        //3 way
        else if (adjacents[1, 0] && !adjacents[1, 2] && adjacents[0, 1] && adjacents[2, 1])
        {
            index = 6;
        }
        else if (adjacents[1, 0] && adjacents[1, 2] && !adjacents[0, 1] && adjacents[2, 1])
        {
            index = 7;
        }
        else if (!adjacents[1, 0] && adjacents[1, 2] && adjacents[0, 1] && adjacents[2, 1])
        {
            index = 8;
        }
        else if (adjacents[1, 0] && adjacents[1, 2] && adjacents[0, 1] && !adjacents[2, 1])
        {
            index = 9;
        }
        //4 way
        else if (adjacents[1, 0] && adjacents[1, 2] && adjacents[0, 1] && adjacents[2, 1])
        {
            index = 10;
        }
        //1 way
        else if (adjacents[1, 0] && !adjacents[1, 2] && !adjacents[0, 1] && !adjacents[2, 1])
        {
            index = 11;
        }
        else if (!adjacents[1, 0] && adjacents[1, 2] && !adjacents[0, 1] && !adjacents[2, 1])
        {
            index = 12;
        }
        else if (!adjacents[1, 0] && !adjacents[1, 2] && adjacents[0, 1] && !adjacents[2, 1])
        {
            index = 13;
        }
        else if (!adjacents[1, 0] && !adjacents[1, 2] && !adjacents[0, 1] && adjacents[2, 1])
        {
            index = 14;
        }

        if(index != -1)
        {
            spRend.sprite = corridorSprites[index];
            transform.localScale = scale[index];
            bc.size = bcScale[index];
        }
    }
}
