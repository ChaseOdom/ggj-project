using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeHelper : MonoBehaviour
{
    public SpikeRoom InnerSpikes;

    private void OnMouseDown()
    {
        if (GameManager.currGameState == GameState.DUNGEON_DEFENSE)
        {
            InnerSpikes.SpikesPressed();
        }
    }
}
