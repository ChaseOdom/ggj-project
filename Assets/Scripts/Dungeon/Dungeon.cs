using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dungeon : MonoBehaviour
{
    public Camera cam;
    public Tile[,] grid;
    public GameObject backgroundTile;
    public List<Room> rooms;
    public bool draggingRoom = false;
    public bool drawingCorridors = false;
    public bool drawingBackgrounds = false;
    public bool editMode = false;
    public bool destroyRoomMode = false;
    public bool placingRoom = false;
    public bool playing = true;
    public int gridX;
    public int gridY;
    public int offsetX;  //grid starts 8 units to the left of origin 8
    public int offsetY;  //grid starts 5 units above origin -5
    public DungeunUtils utils;

    private void FixedUpdate()
    {
        if (!Input.GetMouseButton(0))
        {
            drawingBackgrounds = false;
            drawingCorridors = false;
            draggingRoom = false;
        }
    }

    private void Awake()
    {
        grid = new Tile[gridX, -1*gridY];

        for (int i = 0; i < gridX; i++)
        {
            for(int j = 0; j < -gridY; j++)
            {
                GameObject g = Instantiate(backgroundTile, transform);
                Tile t = g.AddComponent<Tile>();
                t.tileAsset = g;
                t.coord = new Vector2(i, -1*j);
                grid[i, j] = t;
                g.transform.position = utils.CoordPosToWorldPos(new Vector2(i, -1*j));
            }
        }

        foreach(Room r in rooms)
        {
            r.dungeon = this;
            r.DisableTiles();
        }
    }
}