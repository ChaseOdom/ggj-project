using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    List<List<Tile>> grid;
    public SavedDungeonLayout initialDungeon;
    public Corridor corridorPrefab;
    public Background backgroundPrefab;
    public Tile tilePrefab;

    //The amount of rows below the origin
    private int yOffset = 0;

    // Start is called before the first frame update
    void Start()
    {
        //instantiate grid with background tiles
        grid = new List<List<Tile>>();
        for(int y = 0; y < initialDungeon.ySize; y++)
        {
            grid.Add(new List<Tile>());
            for(int x = 0; x < initialDungeon.xSize; x++)
            {
                Tile t = Instantiate<Tile>(tilePrefab, transform);
                t.transform.position = t.transform.position + new Vector3(x, y, 0);
                grid[y].Add(t);
                PutBackground(x,y);
            }
        }
        
        foreach(Coords coords in initialDungeon.NewCorridors)
        {
            PutCorridor(coords.x, coords.y);
        }
    }

    //change a corridor in the grid to a background tile
    public void RemoveCorridor(int x, int y)
    {
        if (GetTile(x, y).isCorridor)
        {
            PutBackground(x, y);
        }
    }

    //put a background at the listed coordinates
    public void PutBackground(int x, int y)
    {
        Tile t = GetTile(x, y);
        t.ResetTile();
        Background b = Instantiate(backgroundPrefab, t.transform);
        t.tileAsset = b.gameObject;
        t.isBackground = true;
    }

    //put a corridor at the listed coordinates
    public void PutCorridor(int x, int y)
    {
        Tile t = GetTile(x, y);
        t.ResetTile();
        Corridor c = Instantiate(corridorPrefab, t.transform);
        t.tileAsset = c.gameObject;
        t.isCorridor = true;
    }

    //return if the coordinates listed are valid coordinates inside the grid
    public bool HasTile(int x, int y)
    {
        return grid[y - yOffset] != null && grid[y - yOffset][x];
    }

    //get a tile from the grid at the specified coordinates
    //if not present return null
    public Tile GetTile(int x, int y)
    {
        if(grid[y - yOffset] != null)
        {
            return grid[y - yOffset][x];
        }
        return null;
    }

    //Adds a room to the grid
    //coord: the coordinate of the top right room
    //dimensions: the dimensions of the room
    public void AddRoom(Vector2 coord, Vector2 dimensions)
    {

    }
}

[System.Serializable]
public class Coords
{
    public int x;
    public int y;
}