using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootPile : MonoBehaviour, ILootSource
{
    PlayerResourceManager resources;
    public int GearAmount()
    {
        if (resources == null)
            resources = FindObjectOfType<PlayerResourceManager>();
        return resources.GetResource("Gear").val;
    }

    public int GoldAmount()
    {
        if (resources == null)
            resources = FindObjectOfType<PlayerResourceManager>();
        return resources.GetResource("Gold").val;
    }

    public int TakeGear(int amount)
    {
        if (amount > GearAmount())
            amount = GearAmount();
        resources.GetResource("Gear").Consume(amount);
        return amount;
    }

    public int TakeGold(int amount)
    {
        if (amount > GoldAmount())
            amount = GoldAmount();
        resources.GetResource("Gold").Consume(amount);
        return amount;
    }

    // Start is called before the first frame update
    private void Awake()
    {
        resources = FindObjectOfType<PlayerResourceManager>();
    }

    private void Start()
    {
        GetComponentInParent<Room>().AddLoot(this);
    }
}
