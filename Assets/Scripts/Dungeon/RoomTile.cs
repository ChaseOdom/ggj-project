using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTile : MonoBehaviour
{
    public Vector2 LocalCoord;

    public Sprite doorSprite;
    public Sprite wallSprite;
    DungeunUtils utils => DungeunUtils.instance;
    SpriteRenderer spRend;

    private void Start()
    {
        spRend = GetComponent<SpriteRenderer>();
    }

    public void CheckForCorridors()
    {
        if (GetComponent<Tile>().isDoor)
        {
            spRend = GetComponent<SpriteRenderer>();
            IEnumerable<Tile> adjacents = utils.GetAdjacentTiles(GetComponent<Tile>());
            bool corridorFound = false;
            foreach (Tile t in adjacents)
            {
                if (t.isCorridor)
                {
                    corridorFound = true;
                }
                //if connected to a door that doesn't belong to this room
                if(t.isDoor && t.GetComponentInParent<Room>().coord != this.GetComponentInParent<Room>().coord)
                {
                    corridorFound = true;
                }
            }
            if (corridorFound)
            {
                spRend.sprite = doorSprite;
            }
            else
            {
                spRend.sprite = wallSprite;
            }
        }
    }
}
