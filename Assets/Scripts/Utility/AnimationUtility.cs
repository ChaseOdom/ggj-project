using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationUtility
{
    public static Coroutine DoAnimation(MonoBehaviour on, float time, AnimationCurve curve, System.Action<float> step, bool unscaledTime = false, System.Action onComplete = null)
    {
        return on.StartCoroutine(Animation(time, curve, step, unscaledTime, onComplete));
    }

    public static IEnumerator Animation(float time, AnimationCurve curve, System.Action<float> step, bool unscaledTime = false, System.Action onComplete = null)
    {
        float timePassed = 0f;
        while (timePassed < time)
        {
            step.Invoke(curve.Evaluate(timePassed / time));
            yield return null;
            timePassed += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
        }
        step.Invoke(curve.Evaluate(1));
        if (onComplete != null)
            onComplete.Invoke();
    }

    public static AnimationCurve linearAnimation => AnimationCurve.Linear(0, 0, 1, 1);
    public static AnimationCurve easeInOutAnimation => AnimationCurve.EaseInOut(0, 0, 1, 1);
    public static AnimationCurve exponentialAnimation => new AnimationCurve(new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 2, 2));
}
