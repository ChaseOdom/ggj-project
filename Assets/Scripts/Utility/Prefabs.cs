using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Prefab Library")]
public class Prefabs : ScriptableObject
{
    public AudioPlayer audioPlayerPrefab;
    public UnityEngine.Audio.AudioMixer masterMixer;
    public ResourceEyeCandy resourceEyeCandyPrefab;
    public TutorialPopup tutorialPopupPrefab;
    public ErrorBanner errorBanner;
    [SerializeField] AdventurerNames adventurerNames;
    public Adventurer adventurerPrefab;
    public DungeonResourceDrop resourceDropPrefab;
    public List<string> adventurerNameList => adventurerNames.names;

    static Prefabs _library;
    public static Prefabs library
    {
        get
        {
            if (_library == null)
                _library = Resources.Load<Prefabs>("Prefab Library");

            return _library;
        }
    }
}
