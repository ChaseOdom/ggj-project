using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindingTester : MonoBehaviour
{
    public Transform destination;
    public DungeonPathfinder pathfinder;
    public float speed;

    private void Update()
    {
        pathfinder.destination = destination.position;
        pathfinder.speed = speed;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 0.3f);
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(destination.position, 0.3f);
    }
#endif
}
