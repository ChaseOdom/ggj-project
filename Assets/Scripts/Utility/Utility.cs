using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility
{
    public static Vector2 mouseWorldPos => Camera.main.ScreenToWorldPoint(Input.mousePosition);

    public static T RandomFromArray<T>(T[] array)
    {
        int selected = Random.Range(0, array.Length);
        return array[selected];
    }

    public static T RandomFromList<T>(List<T> list)
    {
        if (list.Count == 0)
        {
            return default(T);
        }
        int selected = Random.Range(0, list.Count);
        return list[selected];
    }

    public static Vector2 ProportionalPivot(Sprite sprite)
    {
        Vector2 pivot = sprite.pivot;
        pivot.x /= sprite.rect.width;
        pivot.y /= sprite.rect.height;
        return pivot;
    }

    public static bool VectorMagnitudeLTE(Vector2 vector, float magnitude)
    {
        return vector.sqrMagnitude <= Mathf.Pow(magnitude, 2);
    }
}
