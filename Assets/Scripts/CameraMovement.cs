using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Camera cam;
    public int minZoom;
    public int maxZoom;

    private void Start()
    {
        AudioMaster.SetVolume(AUDIO_BUS.MASTER, 0f);
    }

    private void FixedUpdate()
    {
        cam.orthographicSize += Input.mouseScrollDelta.y;
        if(cam.orthographicSize < minZoom)
        {
            cam.orthographicSize = minZoom;
        }
        else if(cam.orthographicSize > maxZoom)
        {
            cam.orthographicSize = maxZoom;
        }

        Vector3 pos = transform.position;
        pos.x += Input.GetAxis("Horizontal");
        pos.y += Input.GetAxis("Vertical");
        transform.position = pos;
    }
}
